﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FinalCheck3
{
    static class Program
    {
        //Public objects
        public static TestEquipment.epluseEE31 sensorEE31;    //EE31 Referance sensor
        public static TestEquipment.HMP234 sensorHMP;     //HMP234 Referance Sensor
        public static TestEquipment.YoungBaro sensorBaro;     //Referance Sensor
        public static txDecoder.decoderBell202 decoderBell202;    //Bell202 modem decoder
        public static iMet_1_Decoder.iMet1_Decoder decoderIMet1;  //iMet-1 decoder
        //public static Radiosonde.iMet1 decoderIMet1Old;         //The old decoder class... Not sure what is going on here.
        public static iMet_1_Decoder.DecoderData currentiMet1Data;

        public static Universal_Radiosonde.iMet1UV2 radiosondeMFGPort; //Manufacturing data port connection.
        public static Universal_Radiosonde.iMet1UV2 radiosondeRSBCable; //RSB Cable
        public static MFGPrinting.printerSNLabel radiosondeSNLabel;     //Printer for radiosonde serial number label
        public static MFGPrinting.printerSNLabel radiosondeBoxLabel;     //Printer for labeling boxes.
        public static updateCreater.objectUpdate debugUpdate;

        //Sim Referance Readings
        public static bool programMode;             //Sets program to use simulated PTU sensors.
        public static int simPressure = 0;      //Default Pressure start point.
        public static int simAirTemp = 0;        //Default Air Temp start point.
        public static int simHumidity = 0;        //Default Humidty Start Point.

        //Current test settings.
        public static FinalTest.testParameters currentTestSettings;     //Default test settings. This will be saved at the end of the run.
        public static programSettings currentProgramSettings;
        public static string logFile = AppDomain.CurrentDomain.BaseDirectory + "logFile.log";
        public static string settingsFile = AppDomain.CurrentDomain.BaseDirectory + "finalTestSettings.cfg";
        public static string sensorCorFile = AppDomain.CurrentDomain.BaseDirectory + "730033.0001-01.coef";
        public static string currentWOLog = "";     //The current log file for the work order.

        //Display forms.
        public static frmMain mainDisplay;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadExit += new EventHandler(Application_ThreadExit);

            if (args.Length > 0)    //Checking to see if there are startup calls
            {
                startupSettings(args[0]);
            }


            //Starting program
            programLog("Program Started");

            //Loading settings into the program.
            programLog("Loading program settings.");
            string[] settingFileData = System.IO.File.ReadAllLines(settingsFile); //Loading in all the file data

            currentProgramSettings = loadProgramSettings(settingFileData);
            currentTestSettings = loadTestSettings(settingFileData);
            connectToHardware();    //Setting up the sensors, printer, and decoders.

            if(programMode)
            {
                //Bellow is test code and should only be used for development.
                /*
                System.ComponentModel.BackgroundWorker runTest = new System.ComponentModel.BackgroundWorker();
                runTest.DoWork += new System.ComponentModel.DoWorkEventHandler(runTest_DoWork);
                runTest.RunWorkerAsync();
                programLog("Starting Sim TU Referance Sensors.");

                System.ComponentModel.BackgroundWorker runTestPr = new System.ComponentModel.BackgroundWorker();
                runTestPr.DoWork += new System.ComponentModel.DoWorkEventHandler(runTestPr_DoWork);
                runTestPr.RunWorkerAsync();
                programLog("Starting Sim Pr Referance Sensors.");
                 */
                System.Threading.Thread runTest = new System.Threading.Thread(runTest_DoWork);
                System.Threading.Thread runTestPr = new System.Threading.Thread(runTestPr_DoWork);

                runTest.Start();
                runTestPr.Start();
            }


            //Setting up main display object
            
            try
            {
                mainDisplay = new frmMain();
                
            }
            catch (Exception ex)
            {
                programLog("Main Form Error: " + ex.Message);
                return;
            }

            Application.Run(mainDisplay);   //Setting up the main form.
            

            //frmSettings testSettings = new frmSettings();
            //Application.Run(testSettings);
        }

        private static void startupSettings(string incoming)
        {
            string[] startUp = incoming.Split('-');

            foreach (string line in startUp)
            {
                if (line.Contains("debug")) //Checking to see if we are in debug mode.
                {
                    //Calling for the setup of the debug window.
                    showDebugWindow();
                }

                if (line.Contains("simsensor"))
                {
                    programMode = true; 
                }

                if (line.ToLower().Contains("pressure"))
                {
                    string[] breakDown = line.Split(',');
                    simPressure = Convert.ToInt32(breakDown[1]);
                }
                
                if (line.ToLower().Contains("airtemp"))
                {
                    string[] breakDown = line.Split(',');
                    simAirTemp = Convert.ToInt32(breakDown[1]);
                }

                if (line.ToLower().Contains("humidity"))
                {
                    string[] breakDown = line.Split(',');
                    simHumidity = Convert.ToInt32(breakDown[1]);
                }
                  
                  
            }
        }

        public static void showDebugWindow()
        {
            TextBoxTraceListener tbtl = new TextBoxTraceListener();
            System.Diagnostics.Debug.Listeners.Add(tbtl);

            debugUpdate = new updateCreater.objectUpdate();

            frmDebug curDebug = new frmDebug();
            curDebug.Show();
        }

        public static void connectToHardware()
        {
            //Setting up E+E sensor
            try
            {
                programLog("Starting the e+e referance sensor");
                sensorEE31 = new TestEquipment.epluseEE31(currentProgramSettings.sensorEE31);
                sensorEE31.sensorError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(sensorError_PropertyChange);
                sensorEE31.setAirTempBias(currentTestSettings.offsetAirTemp);
                sensorEE31.setHumidityBias(currentTestSettings.offsetHumidity);

                if (simAirTemp == 0 && simHumidity == 0)
                {
                    //If we are runing the real program then start processing the data.
                    programLog("Starting E+E internal processing.");
                    sensorEE31.startIntProcess(500);
                }
                else
                {
                    
                }


            }
            catch (Exception ex)
            {
                programLog("E+E Startup Error: " + ex.Message);
            }

            //Setup HMP234
            try
            {
                sensorHMP = new TestEquipment.HMP234(currentProgramSettings.sensorHMP234);
            }
            catch (Exception ex)
            {
                programLog("HMP234 Startup Error: " + ex.Message);
            }

            //Setup Baro
            try
            {
                sensorBaro = new TestEquipment.YoungBaro(currentProgramSettings.sensorBaro);
                sensorBaro.sensorError.PropertyChange +=new updateCreater.objectUpdate.PropertyChangeHandler(sensorErrorP_PropertyChange);
                sensorBaro.setBias(currentTestSettings.offsetPressure);

                if (!programMode)
                {
                    sensorBaro.startIntProcess();
                }

            }
            catch (Exception ex)
            {
                programLog("Baro Startup Error: " + ex.Message);
            }

            //Setup bell202 decoder
            try
            {
                decoderBell202 = new txDecoder.decoderBell202(currentProgramSettings.decoderBell202);
            }
            catch (Exception ex)
            {
                programLog("Bell202 Decoder Startup Error: " + ex.Message);
            }

            //Setup iMet-1 decoder
            try
            {
                decoderIMet1 = new iMet_1_Decoder.iMet1_Decoder(currentProgramSettings.decoderIMet1);

                //decoderIMet1Old = new Radiosonde.iMet1(currentProgramSettings.decoderIMet1);

            }
            catch (Exception ex)
            {
                programLog("iMet-1 Decoder Startup Error: " + ex.Message);
            }

            //Setup radiosonde RSB Cable
            try
            {
                radiosondeRSBCable = new Universal_Radiosonde.iMet1UV2();
                radiosondeRSBCable.errorConnect.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(error_PropertyChange);
                radiosondeRSBCable.portStart(currentProgramSettings.mfgConAuxCable);
            }
            catch (Exception ex)
            {
                programLog("Radiosonde RSB Cable Error: " + ex.Message);
            }

            //Setup radiosonde MFG Connection
            try
            {
                radiosondeMFGPort = new Universal_Radiosonde.iMet1UV2(currentProgramSettings.mfgConMFGConfig);
            }
            catch (Exception ex)
            {
                programLog("Radiosonde MFG Port Error: " + ex.Message);
            }

            //Setting up the sn printer.
            setupSNPrinter();

            //Setting up the Box label
            try
            {
                radiosondeBoxLabel = new MFGPrinting.printerSNLabel();
                radiosondeBoxLabel.setPrinter(Program.currentProgramSettings.printerShipping);
            }
            catch (Exception prt)
            {
                programLog("Box Label Printer: " + prt.Message);
            }
        }

        //Error for the radiosonde axu/mfg cable.
        static void error_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incoming = (Exception)data.NewValue;
            MessageBox.Show("Aux/MFG Error: " + incoming.Message + "\nPlease check settings and restart.", "Aux/MFG Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        static void sensorError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incoming = (Exception)data.NewValue;
            MessageBox.Show("E+E Error: " + incoming.Message + "\nPlease check settings and restart.","E+E Error", MessageBoxButtons.OK,MessageBoxIcon.Error);
        }

        static void sensorErrorP_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incoming = (Exception)data.NewValue;
            MessageBox.Show("Paro Error: " + incoming.Message + "\nPlease check settings and restart.", "Paro Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /*   This is to support the old IMS TX transmittion scheme

        public static void decoderPacketDataReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            System.Diagnostics.Debug.WriteLine("iMet1 Old data received.");
            currentiMet1Data.packetTime = DateTime.Now;

            try
            {
                currentiMet1Data.pressure = Convert.ToDouble(decoderIMet1Old.getPressure());
                currentiMet1Data.temperature = Convert.ToDouble(decoderIMet1Old.getAirTemp());
                currentiMet1Data.humidity = Convert.ToDouble(decoderIMet1Old.getRH());

                currentiMet1Data.latitude = Convert.ToDouble(decoderIMet1Old.getGPSlat());
                currentiMet1Data.longitude = Convert.ToDouble(decoderIMet1Old.getGPSLong());
                currentiMet1Data.altitude = Convert.ToDouble(decoderIMet1Old.getGPSAlt())/1000;

                currentiMet1Data.Sonde_ID = decoderIMet1Old.getSerialNum();
                currentiMet1Data.Sonde_SN = decoderIMet1Old.getSerialNum();
                currentiMet1Data.FirmwareVersion = decoderIMet1Old.getFirmwareVersion();
            }
            catch(Exception errorE)
            {
                System.Diagnostics.Debug.WriteLine(errorE.Message);

            }
        }
        */

        public static void setupSNPrinter()
        {
            //Setting up the serial number printer.
            try
            {
                radiosondeSNLabel = new MFGPrinting.printerSNLabel();
                radiosondeSNLabel.setPrinter(Program.currentProgramSettings.printerSN);
                radiosondeSNLabel.setSNMode();
            }
            catch (Exception prt)
            {
                programLog("SN Label Printer: " + prt.Message);
            }
        }

        static void runTest_DoWork()//object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            Random humRandom = new Random();

            while (true)
            {
                double air = (Convert.ToDouble(humRandom.Next(simAirTemp, simAirTemp + 20)) / 100) + Program.currentTestSettings.offsetAirTemp;
                double hum = (Convert.ToDouble(humRandom.Next(simHumidity, simHumidity + 40)) / 100) + Program.currentTestSettings.offsetHumidity;
                

                sensorEE31.testHumidityInput(hum);
                sensorEE31.testTempInput(air);

                System.Threading.Thread.Sleep(500);
            }
        }

        static void runTestPr_DoWork()//object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            Random humRandom = new Random();

            while (true)
            {

                double pressure = Convert.ToDouble(humRandom.Next(simPressure, simPressure + 30)) / 100;

                sensorBaro.testPressureInput(pressure);

                System.Threading.Thread.Sleep(2500);
            }
        }

        static void Application_ThreadExit(object sender, EventArgs e)
        {
            
        }

        static void updateRealTimeDebug(string message)
        {
            debugUpdate.UpdatingObject = DateTime.Now.ToString("yyyyMMdd-HH:mm:ss.ffff") + "," + message;
        }

        public static programSettings loadProgramSettings(string[] settingFileData)
        {
            programSettings tempPrgSettings = new programSettings();

            for (int i = 0; i < settingFileData.Length; i++)    //Going through the data.
            {
                if (!settingFileData[i].Contains('#') && settingFileData[i] != "")  //Filtering out notes and blank lines.
                {
                    string[] lineBreak = settingFileData[i].Split('=', ',');    //Breaking each line down to the elements.

                    switch (lineBreak[0].ToLower())  //First sort.
                    {
                        //Referance sensor object settings for testing below.
                        case "sensor":  //Referance sensors object settings.
                            switch (lineBreak[1].ToLower())
                            {
                                case "tu":  //Temp and humidity sensors.
                                    switch (lineBreak[2])
                                    {
                                        case "EE31":
                                            tempPrgSettings.sensorEE31 = lineBreak[3];
                                            break;

                                        case "HMP234":
                                            tempPrgSettings.sensorHMP234 = lineBreak[3];
                                            break;
                                    }
                                    break;

                                case "p":    //Pressure sensors.
                                    tempPrgSettings.sensorBaro = lineBreak[3];
                                    break;

                            }
                            break;

                        case "decoder":
                            switch (lineBreak[1].ToLower())
                            {
                                case "bell202":
                                    tempPrgSettings.decoderBell202 = lineBreak[2];
                                    break;

                                case "imet1":
                                    tempPrgSettings.decoderIMet1 = lineBreak[2];
                                    break;
                            }
                            break;

                        case "mfgconn":
                            switch (lineBreak[1].ToLower())
                            {
                                case "auxcable":
                                    tempPrgSettings.mfgConAuxCable = lineBreak[2];
                                    break;

                                case "mfgconfig":
                                    tempPrgSettings.mfgConMFGConfig = lineBreak[2];
                                    break;
                            }
                            break;

                        //System settings, like logging and printer settings.
                        case "printer":
                            switch (lineBreak[1].ToLower())
                            {
                                case "snlabel":
                                    tempPrgSettings.printerSN = lineBreak[2];
                                    break;

                                case "shipping":
                                    tempPrgSettings.printerShipping = lineBreak[2];
                                    break;
                            }

                            break;

                        case "masterlog":
                            tempPrgSettings.masterLog = lineBreak[1];
                            break;

                        case "xcalloc":
                            tempPrgSettings.xcalLOC = lineBreak[1];
                            break;

                        case "logbasedir":
                            tempPrgSettings.baseDirWOLog = lineBreak[1];
                            break;

                    }
                }
            }

            return tempPrgSettings;
        }

        public static FinalTest.testParameters loadTestSettings(string[] settingFileData)
        {
            FinalTest.testParameters tempTestSettings = new FinalTest.testParameters();

            for (int i = 0; i < settingFileData.Length; i++)    //Going through the data.
            {
                if (!settingFileData[i].Contains('#') && settingFileData[i] != "")  //Filtering out notes and blank lines.
                {
                    string[] lineBreak = settingFileData[i].Split('=', ',');    //Breaking each line down to the elements.

                    switch (lineBreak[0].ToLower())  //First sort.
                    {
                        //Radiosonde test settings below.
                        case "tol":
                            switch (lineBreak[1].ToLower())
                            {
                                case "pressure":
                                    tempTestSettings.tolPressure = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "airtemp":
                                    tempTestSettings.tolAirTemp = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "humidity":
                                    tempTestSettings.tolHumidity = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "horizonal":
                                    tempTestSettings.tolHorizonal = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "vertical":
                                    tempTestSettings.tolVertical = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }

                            break;

                        case "numberofdiffreadingavg":
                            tempTestSettings.numberOfCorrect = Convert.ToInt16(lineBreak[1]);
                            break;

                        case "maxtimetostabilize":
                            tempTestSettings.maxTimeToStablize = Convert.ToInt16(lineBreak[1]);
                            break;

                        case "offset":
                            switch (lineBreak[1].ToLower())
                            {
                                case "pressure":
                                    tempTestSettings.offsetPressure = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "airtemp":
                                    tempTestSettings.offsetAirTemp = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "humidity":
                                    tempTestSettings.offsetHumidity = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }
                            break;

                        case "station":
                            switch (lineBreak[1].ToLower())
                            {
                                case "lat":
                                    tempTestSettings.stationLat = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "long":
                                    tempTestSettings.stationLong = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "alt":
                                    tempTestSettings.stationAlt = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }
                            break;

                        case "maxstability":
                            switch (lineBreak[1].ToLower())
                            {
                                case "pressure":
                                    tempTestSettings.MSLrefP = Convert.ToDouble(lineBreak[2]);
                                    tempTestSettings.MSLsondeP = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "airtemp":
                                    tempTestSettings.MSLrefT = Convert.ToDouble(lineBreak[2]);
                                    tempTestSettings.MSLsondeT = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "humidity":
                                    tempTestSettings.MSLrefU = Convert.ToDouble(lineBreak[2]);
                                    tempTestSettings.MSLsondeU = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }
                            break;
                    }
                }
            }

            return tempTestSettings;
        }


        /// <summary>
        /// Method loads in
        /// </summary>
        static void loadSettings()
        {
            string[] settingFileData = System.IO.File.ReadAllLines("finalTestSettings.cfg"); //Loading in all the file data

            for (int i = 0; i < settingFileData.Length; i++)    //Going through the data.
            {
                if (!settingFileData[i].Contains('#') && settingFileData[i] != "")  //Filtering out notes and blank lines.
                {
                    string[] lineBreak = settingFileData[i].Split('=', ',');    //Breaking each line down to the elements.

                    switch(lineBreak[0].ToLower())  //First sort.
                    {
                        //Referance sensor object settings for testing below.
                        case "sensor":  //Referance sensors object settings.
                            switch (lineBreak[1].ToLower())
                            {
                                case "tu":  //Temp and humidity sensors.
                                    switch(lineBreak[2])
                                    {
                                        case "EE31":
                                            currentProgramSettings.sensorEE31 = lineBreak[3];
                                            break;

                                        case "HMP234":
                                            currentProgramSettings.sensorHMP234 = lineBreak[3];
                                            break;
                                    }
                                    break;

                                case "p":    //Pressure sensors.
                                    currentProgramSettings.sensorBaro = lineBreak[3];
                                    break;

                            }
                            break;

                        case "decoder":
                            switch (lineBreak[1].ToLower())
                            {
                                case "bell202":
                                    currentProgramSettings.decoderBell202 = lineBreak[2];
                                    break;

                                case "imet1":
                                    currentProgramSettings.decoderIMet1 = lineBreak[2];
                                    break;
                            }
                            break;

                        case "mfgconn":
                            switch (lineBreak[1].ToLower())
                            {
                                case "auxcable":
                                    currentProgramSettings.mfgConAuxCable = lineBreak[2];
                                    break;

                                case "mfgconfig":
                                    currentProgramSettings.mfgConMFGConfig = lineBreak[2];
                                    break;
                            }
                            break;

                            //Radiosonde test settings below.

                        case "tol":
                            switch (lineBreak[1].ToLower())
                            {
                                case "pressure":
                                    currentTestSettings.tolPressure = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "airtemp":
                                    currentTestSettings.tolAirTemp = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "humidity":
                                    currentTestSettings.tolHumidity = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "horizonal":
                                    currentTestSettings.tolHorizonal = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "vertical":
                                    currentTestSettings.tolVertical = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }

                            break;

                        case "numberofdiffreadingavg":
                            currentTestSettings.numberOfCorrect = Convert.ToInt16(lineBreak[1]);
                            break;

                        case "maxtimetostabilize":
                            currentTestSettings.maxTimeToStablize = Convert.ToInt16(lineBreak[1]);
                            break;

                        case "offset":
                            switch (lineBreak[1].ToLower())
                            {
                                case "pressure":
                                    currentTestSettings.offsetPressure = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "airtemp":
                                    currentTestSettings.offsetAirTemp = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "humidity":
                                    currentTestSettings.offsetHumidity = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }
                            break;

                        case "station":
                            switch (lineBreak[1].ToLower())
                            {
                                case "lat":
                                    currentTestSettings.stationLat = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "long":
                                    currentTestSettings.stationLong = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "alt":
                                    currentTestSettings.stationAlt = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }
                            break;

                        case "maxstability":
                            switch (lineBreak[1].ToLower())
                            {
                                case "pressure":
                                    currentTestSettings.MSLrefP = Convert.ToDouble(lineBreak[2]);
                                    currentTestSettings.MSLsondeP = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "airtemp":
                                    currentTestSettings.MSLrefT = Convert.ToDouble(lineBreak[2]);
                                    currentTestSettings.MSLsondeT = Convert.ToDouble(lineBreak[2]);
                                    break;

                                case "humidity":
                                    currentTestSettings.MSLrefU = Convert.ToDouble(lineBreak[2]);
                                    currentTestSettings.MSLsondeU = Convert.ToDouble(lineBreak[2]);
                                    break;
                            }
                            break;

                            //System settings, like logging and printer settings.

                        case "printer":
                            switch (lineBreak[1].ToLower())
                            {
                                case "snlabel":
                                    currentProgramSettings.printerSN = lineBreak[2];
                                    break;

                                case "shipping":
                                    currentProgramSettings.printerShipping = lineBreak[2];
                                    break;
                            }
                            
                            break;

                        case "masterlog":
                            currentProgramSettings.masterLog = lineBreak[1];
                            break;

                        case "xcalloc":
                            currentProgramSettings.xcalLOC = lineBreak[1];
                            break;

                        case "logbasedir":
                            currentProgramSettings.baseDirWOLog = lineBreak[1];
                            break;

                    }
                }
            }

        }//not used anymore.

        static void loadSensorCorrection()
        {

        }

        public static void editSettings()
        {
            programLog("Opening settings file.");
            frmSettings editSettings = new frmSettings();
            editSettings.Show();
        }

        public static void programLog(string message)
        {
            /*
            try
            {
                System.IO.File.AppendAllText(logFile, DateTime.Now.ToString("yyyyMMdd-HH:mm:ss.ffff") + "," + message + "\r\n");
            }
            catch
            {
                System.Threading.Thread.Sleep(100);
                System.IO.File.AppendAllText(logFile, DateTime.Now.ToString("yyyyMMdd-HH:mm:ss.ffff") + "," + message + "\r\n");
            }
             */

            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("yyyyMMdd-HH:mm:ss.ffff") + "," + message);
                
        }
    }

    /// <summary>
    /// Overall settings for the program
    /// </summary>
    public class programSettings
    {
        public string decoderBell202;
        public string decoderIMet1;

        public string sensorEE31;
        public string sensorHMP234;
        public string sensorBaro;

        public string mfgConAuxCable;
        public string mfgConMFGConfig;

        public string printerSN;
        public string printerShipping;
        public string masterLog;
        public string xcalLOC;
        public string baseDirWOLog = "";     //Folder for the wo logs.
    }

    public class TextBoxTraceListener : System.Diagnostics.TraceListener
    {
        public updateCreater.objectUpdate curDebug;
        public TextBoxTraceListener()
        {
        }

        public override void Write(string msg)
        {

        }

        public override void WriteLine(string msg)
        {
            Program.debugUpdate.UpdatingObject = msg + "\r\n";
        }
    
    }


}

