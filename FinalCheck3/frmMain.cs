﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FinalCheck3
{
    public partial class frmMain : Form
    { 
        //Current items in process
        configRadiosonde currentSondeType;  //The current Radiosonde type being tested.

        //Elements for stability
        EnviromentStability.stabilityEnviroment testEnviroment = new EnviromentStability.stabilityEnviroment();
        List<FinalTest.stabilityElement> stabilityReadings = new List<FinalTest.stabilityElement>();

        //Auto event contollers
        System.Threading.AutoResetEvent radiosondeReceived = new System.Threading.AutoResetEvent(false);
        System.Threading.AutoResetEvent referanceTUReceived = new System.Threading.AutoResetEvent(false);
        System.Threading.AutoResetEvent referancePReceived = new System.Threading.AutoResetEvent(false);

        //Background workers and threads for processing data.
        System.ComponentModel.BackgroundWorker bwProcessingRadiosonde;
        System.ComponentModel.BackgroundWorker bwProcessingReferanceTU;
        System.ComponentModel.BackgroundWorker bwProcessingReferancePr;

        //Current state of radiosonde being tested.
        bool activeRadiosonde = false;
        //System.Timers.Timer detectDisconnect = new System.Timers.Timer(2000);   //Timer to check and see if the decoded radiosonde has disconnected.
        //System.Timers.Timer detectSonde = new System.Timers.Timer(250);    //timer for detecting radiosonde conneted and disconnected.
        System.ComponentModel.BackgroundWorker bwTimerDetectSonde;
        System.Timers.Timer clearGarbage = new System.Timers.Timer(300000);

        //Item related to testing radiosonde.
        FinalTest.radiosondeTest currentTest;

        //Progress bar for the test
        System.Windows.Forms.ProgressBar testProgress;
        System.Windows.Forms.Label timePassed;

        //Current radiosonde type in use.
        string curSondeType;

        delegate void UpdateDisplay();
        delegate void UpdateDifferance(double pressure, double airTemp, double humidity);
        delegate void UpdateButton(string newText);
        delegate void UpdateTestStatus(FinalTest.testResults curResults);
        delegate void passDoubleUpdate(double updateData);
        delegate void passBoolUpdate(bool updateData);

        public frmMain()
        {
            InitializeComponent();
            connectToHardware();    //Starting up the referance sensor connection.
            setupReferanceStability();       //Setting up stability elements.
            startReferanceStability();       //Starting any background workers and threads dealing with stability determinations and validataions.
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //frmMain_Resize(null, null);
            clearGarbage.Elapsed += new System.Timers.ElapsedEventHandler(clearGarbage_Elapsed);
            clearGarbage.Enabled = true;
        }

        //Looking for keyboard commands.
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.Shift | Keys.D))
            {
                Program.showDebugWindow();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        void clearGarbage_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Garbage Collect Started.");
            GC.Collect();
            System.Diagnostics.Debug.WriteLine("Garbage Collect Finished.");
        }

        #region Methods relating to the stability of the radiosonde and referance(s)
        private void setupReferanceStability()
        {
            //Setting up the referance stability elements.
            Program.programLog("Setting up test enviroment stability checking for referance sensors.");
            testEnviroment.addEnviromentElement("referancePressure", Program.currentTestSettings.MSLrefT * -1, Program.currentTestSettings.MSLrefT, 0, Program.currentTestSettings.numberOfCorrect);
            testEnviroment.addEnviromentElement("referanceAirTemp", Program.currentTestSettings.MSLrefT * -1, Program.currentTestSettings.MSLrefT, 0, Program.currentTestSettings.numberOfCorrect);
            testEnviroment.addEnviromentElement("referanceHumidity", Program.currentTestSettings.MSLrefU * -1, Program.currentTestSettings.MSLrefU, 0, Program.currentTestSettings.numberOfCorrect);

            Program.programLog("Setting up stability calculaters for referance sensors.");
            FinalTest.stabilityElement tempItem = new FinalTest.stabilityElement();
            tempItem.stabilityElementSetup("referancePressure", Program.currentTestSettings.numberOfCorrect);
            stabilityReadings.Add(tempItem);
            tempItem = new FinalTest.stabilityElement();
            tempItem.stabilityElementSetup("referanceAirTemp", Program.currentTestSettings.numberOfCorrect);
            stabilityReadings.Add(tempItem);
            tempItem = new FinalTest.stabilityElement();
            tempItem.stabilityElementSetup("referanceHumidity", Program.currentTestSettings.numberOfCorrect);
            stabilityReadings.Add(tempItem);

        }

        private void startReferanceStability()
        {
            Program.programLog("Setting up referance TU stability background worker.");
            bwProcessingReferanceTU = new BackgroundWorker();
            bwProcessingReferanceTU.DoWork += new DoWorkEventHandler(bwProcessingReferanceTU_DoWork);
            bwProcessingReferanceTU.WorkerSupportsCancellation = true;
            Program.programLog("Queing referance TU stability background worker to start.");

            Program.programLog("SEtting up referacne pressure stability background worker.");
            bwProcessingReferancePr = new BackgroundWorker();
            bwProcessingReferancePr.DoWork += new DoWorkEventHandler(bwProcessingReferancePr_DoWork);
            bwProcessingReferancePr.WorkerSupportsCancellation = true;
            Program.programLog("Queing referance pressure stability background worker.");

            Program.programLog("Starting TU worker.");
            bwProcessingReferanceTU.RunWorkerAsync();

            Program.programLog("Starting Pressure worker.");
            bwProcessingReferancePr.RunWorkerAsync();
        }

        private void stopReferanceStability()
        {
            bwProcessingReferanceTU.CancelAsync();
            bwProcessingReferancePr.CancelAsync();
        }

        private void setupRadiosondeStability()
        {
            //Setting up the referance stability elements.
            Program.programLog("Setting up test enviroment stability checking for a radiosonde.");
            testEnviroment.addEnviromentElement("sondePressure", Program.currentTestSettings.MSLsondeP * -1, Program.currentTestSettings.MSLsondeP, 0, Program.currentTestSettings.numberOfCorrect);
            testEnviroment.addEnviromentElement("sondeAirTemp", Program.currentTestSettings.MSLsondeT * -1, Program.currentTestSettings.MSLsondeT, 0, Program.currentTestSettings.numberOfCorrect);
            testEnviroment.addEnviromentElement("sondeHumidity", Program.currentTestSettings.MSLsondeU * -1, Program.currentTestSettings.MSLsondeU, 0, Program.currentTestSettings.numberOfCorrect);

            Program.programLog("Setting up stability calculaters for a radiosonde.");
            FinalTest.stabilityElement tempItem = new FinalTest.stabilityElement();
            tempItem = new FinalTest.stabilityElement();
            tempItem.stabilityElementSetup("sondePressure", Program.currentTestSettings.numberOfCorrect);
            stabilityReadings.Add(tempItem);
            tempItem = new FinalTest.stabilityElement();
            tempItem.stabilityElementSetup("sondeAirTemp", Program.currentTestSettings.numberOfCorrect);
            stabilityReadings.Add(tempItem);
            tempItem = new FinalTest.stabilityElement();
            tempItem.stabilityElementSetup("sondeHumidity", Program.currentTestSettings.numberOfCorrect);
            stabilityReadings.Add(tempItem);
        }

        private void clearRadiosondeStability()
        {
            //Finding and removing radiosonde stability elements.
            for (int i = 0; i < stabilityReadings.Count; i++)
            {
                if (stabilityReadings[i].desciption == "sondePressure") { stabilityReadings.Remove(stabilityReadings[i]); }
                if (stabilityReadings[i].desciption == "sondeAirTemp") { stabilityReadings.Remove(stabilityReadings[i]); }
                if (stabilityReadings[i].desciption == "sondeHumidity") { stabilityReadings.Remove(stabilityReadings[i]); }
            }
            
            //Removing radiosonde test enviroment elements.
            testEnviroment.removeEviromentElement("sondePressure");
            testEnviroment.removeEviromentElement("sondeAirTemp");
            testEnviroment.removeEviromentElement("sondeHumidity");

        }

        private void startRadiosondeStability()
        {
            Program.programLog("Setting up radiosonde stability background worker.");

            System.Threading.Thread sondeStability = new System.Threading.Thread(this.bwProcessingRadiosonde_DoWork);
            sondeStability.Start();
            /*
            bwProcessingRadiosonde = new BackgroundWorker();
            bwProcessingRadiosonde.DoWork += new DoWorkEventHandler(bwProcessingRadiosonde_DoWork);
            bwProcessingRadiosonde.WorkerSupportsCancellation = true;

            bwProcessingRadiosonde.RunWorkerAsync();
            */
        }

        private void stopRadiosondeStability()
        {
            /*
            if (bwProcessingRadiosonde != null) //Checking to see if the processor is running.
            {
                Program.programLog("Radiosonde stability working and is active.");
                bwProcessingRadiosonde.CancelAsync();

                bwProcessingRadiosonde.DoWork -= new DoWorkEventHandler(bwProcessingRadiosonde_DoWork);
            }
             */ 
        }

        void bwProcessingReferanceTU_DoWork(object sender, DoWorkEventArgs e)
        {
            Program.programLog("Referance TU stability background worker started.");
            while (!bwProcessingReferanceTU.CancellationPending)
            {
                if (referanceTUReceived.WaitOne(3000))
                {
                    System.Threading.Thread.Sleep(50);  //Waiting for a brief time to allow the next reading to come in be it Humidity or Air Temp.
                    double curAir = 0;
                    double curHum = 0;

                    if(Program.sensorEE31 != null)
                    {
                        curAir = Program.sensorEE31.currentAirTemp;
                        curHum = Program.sensorEE31.currentHumidity;
                    }

                    try
                    {
                        foreach (FinalTest.stabilityElement se in stabilityReadings)
                        {
                            if (se.desciption == "referanceAirTemp") //Updateing the airtemp.
                            {
                                se.addDataPoint(curAir);
                                testEnviroment.checkEnviromentElement("referanceAirTemp", se.getDiff(Program.currentTestSettings.numberOfCorrect));
                            }

                            if (se.desciption == "referanceHumidity")  //Updateing humidity
                            {
                                se.addDataPoint(curHum);
                                testEnviroment.checkEnviromentElement("referanceHumidity", se.getDiff(Program.currentTestSettings.numberOfCorrect));
                            }
                        }
                    }
                    catch(Exception error)
                    {
                        Program.programLog("TU stability bw Error: " + error.Message);
                    }

                }
                else
                {
                    Program.programLog("Error, TU Referance stability has not received a packet in over 3 secs.");
                }

                try
                {
                    BeginInvoke(new UpdateDisplay(this.updateStabilityDisplay));
                }
                catch { }

            }

        }

        void bwProcessingReferancePr_DoWork(object sender, DoWorkEventArgs e)
        {
            Program.programLog("Referance pressure stability background worker started.");
            while (!bwProcessingReferancePr.CancellationPending)
            {
                if (referancePReceived.WaitOne(3500))
                {
                    double curPressure = 0;

                    if (Program.sensorBaro != null)
                    {
                        curPressure = Program.sensorBaro.CurrentPressure;
                        foreach (FinalTest.stabilityElement se in stabilityReadings)
                        {
                            if (se.desciption == "referancePressure") //Updateing the airtemp.
                            {
                                se.addDataPoint(curPressure);
                                testEnviroment.checkEnviromentElement("referancePressure", se.getDiff(Program.currentTestSettings.numberOfCorrect));
                            }
                        }
                    }
                }
                else
                {
                    Program.programLog("Error, Pressure Referance stability has not received a packet in over 3 secs");
                }

                try
                {
                    BeginInvoke(new UpdateDisplay(this.updateStabilityDisplay));
                }
                catch { }

            }
        }

        void bwProcessingRadiosonde_DoWork()//object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;

            Program.programLog("Radiosonde stability background worker started.");
            while (activeRadiosonde)     //Break point if the system wants to cancel.
            {
                if (activeRadiosonde)   //Checking to see if there is a active radiosonde
                {
                    if (radiosondeReceived.WaitOne(3000))    //Waiting for the radiosonde data. If massive delay then error.
                    {
                        double sondePressure = 0;
                        double sondeAirTemp = 0;
                        double sondeHumidity = 0;

                        if (!activeRadiosonde)  //Breaking out if no radiosonde is active.
                        {
                            Program.programLog("No active radiosonde. Breaking out of the Radiosonde stability processing.");
                            break;
                        }

                        switch (currentSondeType.decoder)    //Getting the data from the system.
                        {
                            case "imet1":

                                iMet_1_Decoder.DecoderData tempData = Program.currentiMet1Data;
                                sondePressure = tempData.pressure;
                                sondeAirTemp = tempData.temperature;
                                sondeHumidity = tempData.humidity;
                                
                                break;

                            case "bell202":
                                txDecoder.radiosondePTUData tempPTUData = Program.decoderBell202.getLatestPTU();
                                sondePressure = tempPTUData.pressure;
                                sondeAirTemp = tempPTUData.airTemp;
                                sondeHumidity = tempPTUData.humidity;
                                break;
                        }

                        //Updateing the stability elements.
                        foreach (FinalTest.stabilityElement se in stabilityReadings)
                        {
                            if (se.desciption == "sondePressure")
                            {
                                se.addDataPoint(sondePressure);
                                testEnviroment.checkEnviromentElement("sondePressure", se.getDiff(Program.currentTestSettings.numberOfCorrect));
                            }

                            if (se.desciption == "sondeAirTemp")
                            {
                                se.addDataPoint(sondeAirTemp);
                                testEnviroment.checkEnviromentElement("sondeAirTemp", se.getDiff(Program.currentTestSettings.numberOfCorrect));
                            }

                            if (se.desciption == "sondeHumidity")
                            {
                                se.addDataPoint(sondeHumidity);
                                testEnviroment.checkEnviromentElement("sondeHumidity", se.getDiff(Program.currentTestSettings.numberOfCorrect));
                            }
                        }

                    }
                    else
                    {
                        Program.programLog("Error,Radiosonde has not been received in 3 secs.");
                    }
                }
                else
                {
                    break;
                }
            }

            Program.programLog("Radiosonde stability backgound worker shutting down.");
        }

        #endregion

        #region Methods for working with hardware
        private void connectToHardware()
        {
            //Starting up referance sensors.
            if (Program.sensorEE31 != null)
            {
                Program.sensorEE31.humidityReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(humidityReady_PropertyChange);
                Program.sensorEE31.tempReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(tempReady_PropertyChange);
            }

            if (Program.sensorHMP != null)
            {

            }

            if (Program.sensorBaro != null) //Checking to make sure the sensor is on-line
            {
                Program.sensorBaro.pressureReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(pressureReady_PropertyChange);
            }
            else
            {
                //System should be stopped somehow... Need to be a warning of some kind...
                Program.programLog("Error, Referance Pressure Sensor is Null.");
            }

        }

        void pressureReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                BeginInvoke(new UpdateDisplay(this.updateReferancePressureDisplay));
            }
            catch { }

            string pr = ((double)data.NewValue).ToString();

            //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss.ffff") + " = " + pr);

            referancePReceived.Set();
        }

        void tempReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                BeginInvoke(new UpdateDisplay(this.updateReferanceDisplay));
            }
            catch { }
            referanceTUReceived.Set();
        }

        void humidityReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                BeginInvoke(new UpdateDisplay(this.updateReferanceDisplay));
            }
            catch
            {
            }
        }



        #endregion

        #region Methods for updating the display

        private void updateRadiosondeDisplay()
        {
            //Displaying Radiosonde Data
            switch (currentSondeType.decoder.ToLower())
            {
                case "imet1":
                    iMet_1_Decoder.DecoderData latest = Program.decoderIMet1.getCurrentDecoderData();



                    //Updating the radiosonde display
                    //PTU
                    textBoxCurrentRadiosondePressure.Text = latest.pressure.ToString("0.00");
                    textBoxCurrentRadiosondeAirTemp.Text = latest.temperature.ToString("0.00");
                    textBoxCurrentRadiosondeRH.Text = latest.humidity.ToString("0.00");

                    //GPS
                    textBoxCurrentRadiosondeLat.Text = latest.latitude.ToString();
                    textBoxCurrentRadiosondeLong.Text = latest.longitude.ToString();
                    textBoxCurrentRadiosondeAlt.Text = latest.altitude.ToString();

                    //ID information.
                    textBoxCurrentRadiosondeSerialNumber.Text = latest.Sonde_SN;
                    textBoxCurrentRadiosondeTrackingNumber.Text = latest.Sonde_ID;
                    textBoxCurrentRadiosondeTUNumber.Text = latest.Probe_ID;
                    textBoxCurrentRadiosondeFirmware.Text = latest.FirmwareVersion;

                    //Updating the battery
                    if (latest.battery_voltage > 4)
                    {
                        toolStripStatusLabelSondeBattery.BackColor = Color.LightGreen;
                    }
                    if (latest.battery_voltage < 4 && latest.battery_voltage > 3.5)
                    {
                        toolStripStatusLabelSondeBattery.BackColor = Color.Yellow;
                    }

                    if (latest.battery_voltage < 3.5)
                    {
                        toolStripStatusLabelSondeBattery.BackColor = Color.Red;
                    }

                    toolStripStatusLabelSondeBattery.Text = "VBat: " + latest.battery_voltage.ToString("0.0");

                    if (currentTest != null)
                    {
                        FinalTest.testResults[] results = currentTest.getTestDataResults();
                        if (results[results.Length - 1].statusGPSPOS)
                        {
                            textBoxCurrentRadiosondeLat.BackColor = Color.Green;
                            textBoxCurrentRadiosondeLong.BackColor = Color.Green;
                            textBoxCurrentRadiosondeAlt.BackColor = Color.Green;
                        }
                        else
                        {
                            textBoxCurrentRadiosondeLat.BackColor = Color.Red;
                            textBoxCurrentRadiosondeLong.BackColor = Color.Red;
                            textBoxCurrentRadiosondeAlt.BackColor = Color.Red;
                        }
                    }


                    break;

                case "bell202":
                    txDecoder.radiosondePTUData currentPTU = new txDecoder.radiosondePTUData();
                    txDecoder.radiosondeGPSData currentGPS = new txDecoder.radiosondeGPSData();

                    try
                    {
                        currentPTU = Program.decoderBell202.getLatestPTU();
                        currentGPS = Program.decoderBell202.getLatestGPS();
                    }
                    catch
                    {
                        Program.programLog("Error, Error updating Bell202 display.");
                    }
                     
                    
                    //Updating the radiosonde display
                    //PTU
                    textBoxCurrentRadiosondePressure.Text = currentPTU.pressure.ToString("0.00");
                    textBoxCurrentRadiosondeAirTemp.Text = currentPTU.airTemp.ToString("0.00");
                    textBoxCurrentRadiosondeRH.Text = currentPTU.humidity.ToString("0.00");

                    //GPS
                    textBoxCurrentRadiosondeLat.Text = currentGPS.latitude.ToString();
                    textBoxCurrentRadiosondeLong.Text = currentGPS.longitude.ToString();
                    textBoxCurrentRadiosondeAlt.Text = currentGPS.alt.ToString();


                    //Updating the battery
                    if (currentPTU.batteryVoltage > 4)
                    {
                        toolStripStatusLabelSondeBattery.BackColor = Color.LightGreen;
                    }
                    if (currentPTU.batteryVoltage < 4 && currentPTU.batteryVoltage > 3.5)
                    {
                        toolStripStatusLabelSondeBattery.BackColor = Color.Yellow;
                    }

                    if (currentPTU.batteryVoltage < 3.5)
                    {
                        toolStripStatusLabelSondeBattery.BackColor = Color.Red;
                    }
                    toolStripStatusLabelSondeBattery.Text = "VBat: " + currentPTU.batteryVoltage.ToString("0.0");

                    if (currentTest != null)
                    {
                        FinalTest.testResults[] results = currentTest.getTestDataResults();
                        if (results[results.Length-1].statusGPSPOS)
                        {
                            textBoxCurrentRadiosondeLat.BackColor = Color.Green;
                            textBoxCurrentRadiosondeLong.BackColor = Color.Green;
                            textBoxCurrentRadiosondeAlt.BackColor = Color.Green;
                        }
                        else
                        {
                            textBoxCurrentRadiosondeLat.BackColor = Color.Red;
                            textBoxCurrentRadiosondeLong.BackColor = Color.Red;
                            textBoxCurrentRadiosondeAlt.BackColor = Color.Red;
                        }
                    }


                    break;
            }

            //Updating the display with RSB data if there.
            if (Program.radiosondeRSBCable != null && Program.radiosondeRSBCable.radiosondeConnected)
            {
                textBoxCurrentRadiosondeSerialNumber.Text = Program.radiosondeRSBCable.CalData.SerialNumber;
                textBoxCurrentRadiosondeTrackingNumber.Text = Program.radiosondeRSBCable.CalData.SondeID;
                textBoxCurrentRadiosondeTUNumber.Text = Program.radiosondeRSBCable.CalData.ProbeID;
                textBoxCurrentRadiosondeFirmware.Text = Program.radiosondeRSBCable.StatData.Firmware;
            }
            
        }

        private void clearRadiosondeDisplay()
        {
            //Clearing the radiosonde data.
            textBoxCurrentRadiosondePressure.Text = "";
            textBoxCurrentRadiosondeAirTemp.Text = "";
            textBoxCurrentRadiosondeRH.Text = "";
            textBoxCurrentRadiosondeLat.Text = "";
            textBoxCurrentRadiosondeLong.Text = "";
            textBoxCurrentRadiosondeAlt.Text = "";

            //textBoxCurrentRadiosondeFirmware.Text = "";
            textBoxCurrentRadiosondeSerialNumber.Text = "";
            textBoxCurrentRadiosondeTrackingNumber.Text = "";
            textBoxCurrentRadiosondeTUNumber.Text = "";

            //Clearing diff data.... Something better might be a good idea.
            textBoxCurrentSensorPressureDiff.Text = "";
            textBoxCurrentSensorAirTempDiff.Text = "";
            textBoxCurrentSensorRHDiff.Text = "";

            textBoxCurrentRadiosondeLat.BackColor = Color.White;
            textBoxCurrentRadiosondeLong.BackColor = Color.White;
            textBoxCurrentRadiosondeAlt.BackColor = Color.White;

            toolStripStatusLabelSondeBattery.Text = "No Data";
            toolStripStatusLabelSondeBattery.BackColor = Color.White;
        }

        private void updateReferanceDisplay()
        {
            if (Program.sensorEE31 != null)
            {
                textBoxCurrentSensorAirTemp.Text = Program.sensorEE31.currentAirTemp.ToString("0.00");
                textBoxCurrentSensorRH.Text = Program.sensorEE31.currentHumidity.ToString("0.00");
            }

            //Doing a differance update.
            if (activeRadiosonde)
            {
                updateDifferance();
            }
        }

        private void updateReferancePressureDisplay()
        {
            textBoxCurrentSensorPressure.Text = Program.sensorBaro.CurrentPressure.ToString("0.00");
        }

        private void updateStabilityDisplay()
        {
            try
            {
                foreach (FinalTest.stabilityElement se in stabilityReadings)
                {
                    if (se.desciption == "referanceAirTemp")
                    {
                        textBoxRefTempChange.Text = se.getDiff(Program.currentTestSettings.numberOfCorrect).ToString("0.000");
                        if (Math.Abs(se.getDiff(Program.currentTestSettings.numberOfCorrect)) > Program.currentTestSettings.MSLrefT)
                        {
                            textBoxRefTempChange.BackColor = Color.Red;
                        }
                        else
                        {
                            textBoxRefTempChange.BackColor = Color.LightGreen;
                        }
                    }

                    if (se.desciption == "referanceHumidity")
                    {
                        textBoxRefHumidityChange.Text = se.getDiff(Program.currentTestSettings.numberOfCorrect).ToString("0.000");
                        if (Math.Abs(se.getDiff(Program.currentTestSettings.numberOfCorrect)) > Program.currentTestSettings.MSLrefU)
                        {
                            textBoxRefHumidityChange.BackColor = Color.Red;
                        }
                        else
                        {
                            textBoxRefHumidityChange.BackColor = Color.LightGreen;
                        }
                    }

                    if (se.desciption == "referancePressure")
                    {
                        textBoxRefPressureChange.Text = se.getDiff(Program.currentTestSettings.numberOfCorrect).ToString("0.000");
                        if (Math.Abs(se.getDiff(Program.currentTestSettings.numberOfCorrect)) > Program.currentTestSettings.MSLrefP)
                        {
                            textBoxRefPressureChange.BackColor = Color.Red;
                        }
                        else
                        {
                            textBoxRefPressureChange.BackColor = Color.LightGreen;
                        }
                    }

                    if (se.desciption == "sondePressure")
                    {
                        textBoxSondePressureChange.Text = se.getDiff(Program.currentTestSettings.numberOfCorrect).ToString("0.000");
                        if (Math.Abs(se.getDiff(Program.currentTestSettings.numberOfCorrect)) > Program.currentTestSettings.MSLsondeP)
                        {
                            textBoxSondePressureChange.BackColor = Color.Red;
                        }
                        else
                        {
                            textBoxSondePressureChange.BackColor = Color.LightGreen;
                        }
                    }

                    if (se.desciption == "sondeAirTemp")
                    {
                        textBoxSondeTempChange.Text = se.getDiff(Program.currentTestSettings.numberOfCorrect).ToString("0.000");
                        if (Math.Abs(se.getDiff(Program.currentTestSettings.numberOfCorrect)) > Program.currentTestSettings.MSLsondeT)
                        {
                            textBoxSondeTempChange.BackColor = Color.Red;
                        }
                        else
                        {
                            textBoxSondeTempChange.BackColor = Color.LightGreen;
                        }
                    }

                    if (se.desciption == "sondeHumidity")
                    {
                        textBoxSondeHumidityChange.Text = se.getDiff(Program.currentTestSettings.numberOfCorrect).ToString("0.000");
                        if (Math.Abs(se.getDiff(Program.currentTestSettings.numberOfCorrect)) > Program.currentTestSettings.MSLsondeU)
                        {
                            textBoxSondeHumidityChange.BackColor = Color.Red;
                        }
                        else
                        {
                            textBoxSondeHumidityChange.BackColor = Color.LightGreen;
                        }
                    }
                }
            }
            catch (Exception updateDisplayError)
            {
                System.Diagnostics.Debug.WriteLine(updateDisplayError.Message);
            }


            string testingResults = "";

            testingResults += testEnviroment.checkEnviromentElementCount("referancePressure").ToString();
            testingResults += "," + testEnviroment.checkEnviromentElementCount("referanceAirTemp").ToString();
            testingResults += "," + testEnviroment.checkEnviromentElementCount("referanceHumidity").ToString();

            try
            {
                /*  This should be commented out to check things.
                testingResults += "," + testEnviroment.checkEnviromentElementCount("sondePressure").ToString();
                testingResults += "," + testEnviroment.checkEnviromentElementCount("sondeAirTemp").ToString();
                testingResults += "," + testEnviroment.checkEnviromentElementCount("sondeHumidity").ToString();
                */
            }
            catch (Exception ex)
            {
            }

            //textBoxCurrentRadiosondeSerialNumber.Text = testingResults;
            
        }

        private void updateDifferanceDisplay(double pressure, double airTemp, double humidity)
        {
            textBoxCurrentSensorPressureDiff.Text = pressure.ToString("0.000");
            textBoxCurrentSensorAirTempDiff.Text = airTemp.ToString("0.000");
            textBoxCurrentSensorRHDiff.Text = humidity.ToString("0.000");

            if (Math.Abs(pressure) > Program.currentTestSettings.tolPressure)
            {
                textBoxCurrentSensorPressureDiff.BackColor = Color.Red;
            }
            else
            {
                textBoxCurrentSensorPressureDiff.BackColor = Color.LightGreen;
            }

            if (Math.Abs(airTemp) > Program.currentTestSettings.tolAirTemp)
            {
                textBoxCurrentSensorAirTempDiff.BackColor = Color.Red;
            }
            else
            {
                textBoxCurrentSensorAirTempDiff.BackColor = Color.LightGreen;
            }

            if (Math.Abs(humidity) > Program.currentTestSettings.tolHumidity)
            {
                textBoxCurrentSensorRHDiff.BackColor = Color.Red;
            }
            else
            {
                textBoxCurrentSensorRHDiff.BackColor = Color.LightGreen;
            }
        }

        private void updateButtonDisplay(string newText)
        {
            buttonStartTest.Text = newText;
        }

        private void updateRadiosondeIDDisplay(string newText)
        {
            textBoxCurrentRadiosondeSerialNumber.Text = newText;
            textBoxCurrentRadiosondeTrackingNumber.Text = newText;
            textBoxCurrentRadiosondeTUNumber.Text = newText;
        }

        private void updateToolStrip(string desiredUpdate)
        {
            toolStripStatusLabelMain.Text = desiredUpdate;
        }

        private void updatePassCounter(string desiredUpdate)
        {
            labelPassCounter.Text = desiredUpdate;
        }

        private void updateDisplayProgressBar(bool displayProgressBar)
        {
            if (displayProgressBar)
            {
                panelMainDisplay.Controls.Add(testProgress);
                testProgress.Controls.Add(timePassed);
                timePassed.Location = new Point(testProgress.Width / 2 - timePassed.Size.Width / 2, testProgress.Height / 2 - timePassed.Size.Height / 2);

            }
            else
            {
                panelMainDisplay.Controls.Remove(testProgress);
                testProgress.Controls.Remove(timePassed);
            }
        }

        private void updateProgressBar(double currentProgress)
        {
            testProgress.Value = Convert.ToInt16(currentProgress);
            
            //Updating the to show how much time has passed.
            TimeSpan time = DateTime.Now - currentTest.getTimeStarted();
            timePassed.Text = time.Minutes.ToString("00") + ":" + time.Seconds.ToString("00") + " of " + Program.currentTestSettings.maxTimeToStablize.ToString() + ":00";
            
        }

        #endregion

        #region Methods for controllering the radiosonde type configuration
        private void comboBoxRadiosondeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<configRadiosonde> type = loadRadiosondeConfig();
            for (int i = 0; i < type.Count; i++)
            {
                if (type[i].name == comboBoxRadiosondeType.Text)
                {
                    this.currentSondeType = type[i];
                    if (type[i].name.Contains("RS"))
                    {
                        panelAuxCable.Visible = true;
                    }
                    else
                    {
                        panelAuxCable.Visible = false;
                    }
                }
            }

            //Connecting to decoder.
            switch (this.currentSondeType.decoder.ToLower())
            {
                case "imet1":
                    
                    if (Program.decoderIMet1 != null)
                    {
                        Program.decoderIMet1.packetReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetReady_PropertyChange);
                        Program.decoderIMet1.packetTimeout.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetTimeout_PropertyChange);
                        Program.decoderIMet1.packetBad.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetBad_PropertyChange);

                        if (!Program.decoderIMet1.COMPort.IsOpen)
                        {
                            Program.decoderIMet1.connectToDecoderV2(Program.currentProgramSettings.decoderIMet1);
                            Program.decoderIMet1.startCollectingData();
                        }
                    }
                    else
                    {
                        string errorText = "Error, IMS decoder selected but not avaiable.";
                        Program.programLog(errorText);
                        MessageBox.Show(errorText + "\nCheck settings and restart.", "Decoder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    /*
                    System.Diagnostics.Debug.WriteLine("Opening com port");
                    Program.decoderIMet1Old.Initialize();

                    Program.currentiMet1Data = new iMet_1_Decoder.DecoderData();
                    Program.decoderIMet1Old.decoderPacketDataReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(Program.decoderPacketDataReady_PropertyChange);
                    System.Diagnostics.Debug.WriteLine("Starting collection.");
                    Program.decoderIMet1Old.startDecoderCollection();

                    Program.decoderIMet1Old.decoderPacketDataReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(decoderPacketDataReady_PropertyChange);

                    */
                    break;

                case "bell202":
                    if (Program.decoderBell202 != null)
                    {
                        Program.decoderBell202.packetPTUReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUReady_PropertyChange);
                        Program.decoderBell202.packetPTUXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUXReady_PropertyChange);
                        Program.decoderBell202.packetPTUTimeout.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUTimeout_PropertyChange);
                        Program.decoderBell202.packetGPSReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSReady_PropertyChange);
                        Program.decoderBell202.packetGPSXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSXReady_PropertyChange);
                        Program.decoderBell202.packetGPSTimeout.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSTimeout_PropertyChange);
                        Program.decoderBell202.packetBad.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetBad_PropertyChange);
                    }
                    else
                    {
                        string errorText = "Error, Bell202 decoder selected but not available.";
                        Program.programLog(errorText);
                        MessageBox.Show(errorText + "\nCheck settings and restart.", "Decoder Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    break;
            }

            curSondeType = comboBoxRadiosondeType.Text;

            if (bwTimerDetectSonde == null)
            {
                bwTimerDetectSonde = new BackgroundWorker();
                bwTimerDetectSonde.WorkerSupportsCancellation = true;
                bwTimerDetectSonde.DoWork += new DoWorkEventHandler(bwTimerDetectSonde_DoWork);
                bwTimerDetectSonde.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwTimerDetectSonde_RunWorkerCompleted);
                bwTimerDetectSonde.RunWorkerAsync();
            }
        }

        void bwTimerDetectSonde_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Error Timer has stopped.");
        }

        void bwTimerDetectSonde_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!bwTimerDetectSonde.CancellationPending)
            {

                //Check current time vs latest packet time.
                DateTime compareTime = DateTime.Now;
                switch (currentSondeType.decoder)
                {
                    case "imet1":
                        iMet_1_Decoder.DecoderData tempDataIMS = new iMet_1_Decoder.DecoderData();
                        try
                        {
                            tempDataIMS = Program.decoderIMet1.getCurrentDecoderData();
                            compareTime = tempDataIMS.packetTime;
                        }
                        catch (Exception ex)
                        {
                            Program.programLog("Error, Radiosonde Timing function. " + ex.Message);
                        }
                        break;

                    case "bell202":
                        txDecoder.radiosondePTUData tempData = new txDecoder.radiosondePTUData();
                        try
                        {
                            tempData = Program.decoderBell202.getLatestPTU();   //Pulling in the latest bell202 Data. If there is any.
                            compareTime = tempData.packetDateTime;
                        }
                        catch (Exception ex)
                        {
                            Program.programLog("Error, Radiosonde Timing function. " + ex.Message);
                        }
                        break;
                }

                TimeSpan passed = DateTime.Now - compareTime;

                //System.Diagnostics.Debug.WriteLine("Detect Timer Time - " + compareTime.ToString("HH:mm:ss.ffff") + " = " + passed.TotalSeconds.ToString());

                //If no active sonde and time is less then 1000ms then new sonde connected.
                if (passed.TotalSeconds >= 0.1 && passed.TotalSeconds <= 2.5)
                {
                    //System.Diagnostics.Debug.WriteLine("Sonde Active.");
                }

                //Setting the program to active
                if (!activeRadiosonde && passed.TotalSeconds >= 0.1 && passed.TotalSeconds <= 2.4)
                {
                    activeRadiosonde = true;
                    System.Diagnostics.Debug.WriteLine("New Radiosonde Found");

                    setupRadiosondeStability();
                    startRadiosondeStability();

                    System.Threading.Thread.Sleep(500);

                    if (autoTestToolStripMenuItem.Checked)
                    {
                        if (currentSondeType.decoder == "imet1")
                        {
                            System.Threading.Thread.Sleep(1200);
                        }

                        buttonStartTest_Click(null, null);
                    }
                }

                if (activeRadiosonde && passed.TotalSeconds >= 2.4)//If active sonde and time is greater then 2500ms sonde has disconnected. //Also need to check for active test.
                {
                    Program.programLog("Radiosonde no londer connected. " + currentSondeType.decoder);
                    //Think I need to clear any radiosonde decoder data in buffers.. Please revisit

                    //Stop test if running.
                    if (currentTest != null)
                    {
                        Program.programLog("Radiosonde Disconnected Auto Shudown.");
                        buttonStartTest_Click(null, null);
                    }

                    //Clear radiosondes stability data.
                    stopRadiosondeStability();

                    //Clear differance data.
                    clearRadiosondeStability();

                    //Clear screen data.
                    BeginInvoke(new UpdateDisplay(this.clearRadiosondeDisplay));

                    activeRadiosonde = false;
                    System.Diagnostics.Debug.WriteLine("Active Radiosonde = false");
                }

                System.Threading.Thread.Sleep(500);
            }
        }

        /*
        void detectDisconnect_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (currentTest == null)
            {
                DateTime compareTime = DateTime.Now;
                switch (currentSondeType.decoder)
                {
                    case "imet1":
                        iMet_1_Decoder.DecoderData tempDataIMS = new iMet_1_Decoder.DecoderData();
                        try
                        {
                            tempDataIMS = Program.decoderIMet1.getCurrentDecoderData();
                            compareTime = tempDataIMS.packetTime;
                        }
                        catch (Exception ex)
                        {
                            Program.programLog("Error, Radiosonde Timing function. " + ex.Message);
                        }
                        break;

                    case "bell202":
                        txDecoder.radiosondePTUData tempData = new txDecoder.radiosondePTUData();
                        try
                        {
                            tempData = Program.decoderBell202.getLatestPTU();   //Pulling in the latest bell202 Data. If there is any.
                            compareTime = tempData.packetDateTime;
                        }
                        catch (Exception ex)
                        {
                            Program.programLog("Error, Radiosonde Timing function. " + ex.Message);
                        }
                        break;

                    case "":
                        compareTime = DateTime.Now.AddSeconds(-60);
                        break;
                }

                DateTime currentTime = DateTime.Now;    //Brought in the current time for debuging...

                if (compareTime == null)    //If the time is null then IMS decoder has no new data. Setting the compare time to something that will que a disconnect event.
                {
                    compareTime = DateTime.Now.AddSeconds(-60);
                }



                if ((currentTime - compareTime) > new TimeSpan(0, 0, 2))
                {
                    
                    Program.programLog("Radiosonde no londer connected. " + currentSondeType.decoder);
                    //Think I need to clear any radiosonde decoder data in buffers.. Please revisit

                    //Stop test if running.

                    //Clear radiosondes stability data.
                    stopRadiosondeStability();

                    //Clear differance data.
                    clearRadiosondeStability();

                    //Clear screen data.
                    BeginInvoke(new UpdateDisplay(this.clearRadiosondeDisplay));

                    activeRadiosonde = false;
                    detectDisconnect.Elapsed -= new System.Timers.ElapsedEventHandler(detectDisconnect_Elapsed);    //Shutting down looking for radiosonde until reactivated.
                    System.Diagnostics.Debug.WriteLine("Active Radiosonde = false");
                     
                }
            }

        }
         */

        private void comboBoxRadiosondeType_Click(object sender, EventArgs e)
        {
            comboBoxRadiosondeType.Items.Clear(); //Clearing items and getting ready to reload.
            clearRadiosondeDisplay();
            curSondeType = "";

            //Shutdown event operators connections for decoders.
            try
            {
                switch (currentSondeType.decoder)
                {
                    case "imet1":
                        if (Program.decoderIMet1 != null)
                        {
                            //Removing event connections.
                            Program.decoderIMet1.packetReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetReady_PropertyChange);
                            Program.decoderIMet1.packetTimeout.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetTimeout_PropertyChange);
                            Program.decoderIMet1.packetBad.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetBad_PropertyChange);
                        }
                        break;

                    case "bell202":
                        Program.decoderBell202.packetPTUReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUReady_PropertyChange);
                        Program.decoderBell202.packetPTUXReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUXReady_PropertyChange);
                        Program.decoderBell202.packetPTUTimeout.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUTimeout_PropertyChange);
                        Program.decoderBell202.packetGPSReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSReady_PropertyChange);
                        Program.decoderBell202.packetGPSXReady.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSXReady_PropertyChange);
                        Program.decoderBell202.packetGPSTimeout.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetGPSTimeout_PropertyChange);
                        Program.decoderBell202.packetBad.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(packetBad_PropertyChange);
                        break;
                }

                currentSondeType.decoder = "";
                
            }
            catch (Exception ex)
            {
                Program.programLog("Error shutting down Radiosonde event connection: " + ex.Message);
            }

            List<configRadiosonde> sondeTypes = loadRadiosondeConfig();

            for (int i = 0; i < sondeTypes.Count; i++)
            {
                comboBoxRadiosondeType.Items.Add(sondeTypes[i].name);
            }

            
        }

        private List<configRadiosonde> loadRadiosondeConfig()
        {
            Program.programLog("Loading radiosonde configurations into the program from radiosondeSettings.cfg");
            string[] radiosondeConfigLine = System.IO.File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + "radiosondeSettings.cfg");

            List<configRadiosonde> sondeTypes = new List<configRadiosonde>();

            //Working though the data
            for (int i = 0; i < radiosondeConfigLine.Length; i++)
            {
                if (!radiosondeConfigLine[i].Contains("#") && radiosondeConfigLine[i] != "")    //Filtering out comments and blank lines.
                {
                    string[] lineBreak = radiosondeConfigLine[i].Split(',');    //Breaking down the config line
                    configRadiosonde tempConfig = new configRadiosonde();
                    tempConfig.name = lineBreak[0];
                    tempConfig.optionGPS = Convert.ToBoolean(lineBreak[1]);
                    tempConfig.optionPressure = Convert.ToBoolean(lineBreak[2]);
                    tempConfig.optionCable = Convert.ToBoolean(lineBreak[3]);
                    tempConfig.modeTX = lineBreak[4];
                    tempConfig.testFrequancy = Convert.ToDouble(lineBreak[5]);
                    tempConfig.decoder = lineBreak[6];

                    sondeTypes.Add(tempConfig);
                }
            }

            return sondeTypes;
        }

        #region Radiosonde Data Events.

        void packetGPSTimeout_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //When there is a timing error or problem
            //Program.programLog("Error, " + currentSondeType.decoder + " GPS Data Time Out.");
        }

        void packetGPSXReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                BeginInvoke(new UpdateDisplay(this.updateRadiosondeDisplay));
            }
            catch
            {
            }
        }

        void packetGPSReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            try
            {
                BeginInvoke(new UpdateDisplay(this.updateRadiosondeDisplay));
            }
            catch
            {
            }
        }

        void packetPTUTimeout_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //When there is a timing error or problem
            //Program.programLog("Error, " + currentSondeType.decoder + " PTU Data Time Out.");
        }

        void packetPTUXReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            checkActive();

            radiosondeReceived.Set();   //Radiosonde Data received.

            try
            {
                BeginInvoke(new UpdateDisplay(this.updateRadiosondeDisplay));
            }
            catch
            {
            }
        }

        void packetPTUReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            checkActive();

            radiosondeReceived.Set();   //Radiosonde Data received.

            try
            {
                BeginInvoke(new UpdateDisplay(this.updateRadiosondeDisplay));
            }
            catch
            {
            }
        }


        //iMet-1 Decoder items below
        void packetReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //iMet_1_Decoder.DecoderData incoming = (iMet_1_Decoder.DecoderData)data.NewValue;

            checkActive();

            radiosondeReceived.Set();   //Radiosonde Data received.

            try
            {
                BeginInvoke(new UpdateDisplay(this.updateRadiosondeDisplay));
            }
            catch
            {
            }
        }


        //General Radiosonde Packets.
        void packetBad_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //throw new NotImplementedException();
            Program.programLog("Error, " + currentSondeType.decoder + " Bad Data Packet Decoded.");
        }

        void packetTimeout_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //throw new NotImplementedException();
            Program.programLog("Error, " + currentSondeType.decoder + " Data Pakcet timed out.");
        }

        void decoderPacketDataReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            checkActive();

            radiosondeReceived.Set();   //Radiosonde Data received.

            try
            {
                BeginInvoke(new UpdateDisplay(this.updateRadiosondeDisplay));
            }
            catch
            {
            }
        }

        private void checkActive()
        {
            /*
            if (!activeRadiosonde)
            {
                activeRadiosonde = true;
                System.Diagnostics.Debug.WriteLine("New Radiosonde Found");

                //detectDisconnect.Elapsed += new System.Timers.ElapsedEventHandler(detectDisconnect_Elapsed);    //Setting up the checker for disconnected radiosondes.
                //detectDisconnect.Enabled = true;

                setupRadiosondeStability();
                startRadiosondeStability();

                if (autoTestToolStripMenuItem.Checked)
                {
                    buttonStartTest_Click(null, null);
                }
            }
             */ 
        }

        #endregion

        #endregion

        #region Methods relateding to testing. Starting, Updataing, Stoping items like that
        private void buttonStartTest_Click(object sender, EventArgs e)
        {
            /*
            if (sender == null)
            {
                while (true)
                {
                    if (stabilityReadings.Count <= 3)
                    {
                        System.Threading.Thread.Sleep(250);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            */

            if (buttonStartTest.Text.Contains("Start"))
            {
                Program.programLog("Starting to build new test.");

                //Checking to see if we are ready to start a test.
                if (labelCurrentOperator.Text == "")
                {
                    MessageBox.Show("No username entered.", "Operator Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (labelCurrentWorkOrder.Text == "")
                {
                    MessageBox.Show("No work order entered.", "Work Order Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (activeRadiosonde)   //Starting the test is there is an active radiosonde.
                {
                    //Get serial number if mfg is not connected, and bell202 tx scheme

                    //Setting up the object to remain as a place holder.
                    if (currentTest != null)
                    {
                        Program.programLog("Start Test Error: currentTest is not null");
                    }
                    //currentTest = new FinalTest.radiosondeTest();

                    if (currentSondeType.decoder == "bell202" && Program.radiosondeMFGPort == null && !currentSondeType.name.Contains("RS"))
                    {
                        /*
                        string sondeSN = "";
                        if (Bill.Interfaces.InputBox("Serial Number Input", "Input SN:", ref sondeSN) == DialogResult.OK)
                        {
                            BeginInvoke(new UpdateButton(this.updateRadiosondeIDDisplay), new object[] { sondeSN });
                        }
                         */


                        Bill.Interfaces.InputBoxBW("Serial Number Input", "Input SN:", "");
                        Bill.Interfaces.inputResult.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(inputResult_PropertyChange);
                    }
                    

                    //Changing the test button.
                    BeginInvoke(new UpdateButton(this.updateButtonDisplay), new object[] { "Cancel Test" });

                    //Setting up the test progress bar.
                    Program.programLog("Setting up progress bar.");
                    setupProgressBar();


                    //Compileing the settings for the test.
                    FinalTest.testParameters testSettings = Program.currentTestSettings;
                    testSettings.optionAuxCable = currentSondeType.optionCable;
                    testSettings.optionGPS = currentSondeType.optionGPS;
                    testSettings.optionPressure = currentSondeType.optionPressure;

                    object decoder = null;
                    Universal_Radiosonde.iMet1UV2 cable = null;
                    object sensorP = null;
                    object sensorTU = null;

                    Program.programLog("Collected test settings. (Aux Cable, GPS, Pressure)");

                    //Setting up the test.
                    switch (currentSondeType.decoder)    //Getting the current decoder needed for the test.
                    {
                        case "imet1":
                            //decoder = (object)Program.decoderIMet1Old;
                            decoder = (object)Program.decoderIMet1;
                            break;

                        case "bell202":
                            decoder = (object)Program.decoderBell202;
                            break;
                    }

                    Program.programLog("Test Decoder selected.");

                    if (testSettings.optionAuxCable) { cable = Program.radiosondeRSBCable; } //If testing a cable is needed settup up the connection.

                    Program.programLog("Creating the test object.");
                    currentTest = new FinalTest.radiosondeTest(decoder, cable, Program.sensorBaro, Program.sensorEE31, testSettings, stabilityReadings);

                    currentTest.testError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(testError_PropertyChange);
                    currentTest.testComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(testComplete_PropertyChange);
                    currentTest.updatePacket.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(updatePacket_PropertyChange);
                    Program.programLog("Connected to test object events.");

                    Program.programLog("Starting current test object.");
                    currentTest.startTest();


                    Program.programLog("Test Object started.");

                }
                else
                {
                    MessageBox.Show("No active radiosonde.");
                }
            }
            else
            {
                currentTest.stopTest();
                //currentTest = null;
                BeginInvoke(new UpdateButton(this.updateButtonDisplay), new object[] { "Start Test" });
                Program.programLog("Test Canceled.");
            }

        }

        void inputResult_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            string sn = (string)data.NewValue;
            BeginInvoke(new UpdateButton(this.updateRadiosondeIDDisplay), new object[] { sn });

        }

        private void setupProgressBar()
        {
            testProgress = new ProgressBar();
            testProgress.Size = new Size(panelMainDisplay.Size.Width, 40);
            testProgress.Location = new Point(0, panelMainDisplay.Size.Height - 60);
            testProgress.Maximum = 100;

            //Setting up time label.
            timePassed = new Label();
            timePassed.TextAlign = ContentAlignment.MiddleCenter;
            timePassed.AutoSize = false;
            timePassed.Size = new System.Drawing.Size(150, 25);
            timePassed.Font = new System.Drawing.Font("Arial Bold", 14, System.Drawing.FontStyle.Bold);
            timePassed.ForeColor = Color.Black;

            BeginInvoke(new passBoolUpdate(this.updateDisplayProgressBar), new object[] { true });
        }

        void testError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incoming = (Exception)data.NewValue;
            Program.programLog("Current Test Error: " + incoming.Message);
        }

        void updatePacket_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            FinalTest.testResults incoming = (FinalTest.testResults)data.NewValue;  //Of the moment testing data.
            FinalTest.testResults[] totalTestData = currentTest.getTestDataResults();   //The total test ammount.

            //Breaking down the results and update items.
            int sondeTotalPassCount = currentTest.getTotalPassCount();
            int sondeCurrentPassCount = currentTest.getCurrentPassCount();

            double amountPerUnit = (100 / Convert.ToDouble(sondeTotalPassCount));

            System.Diagnostics.Debug.WriteLine("Update number: " + amountPerUnit * sondeCurrentPassCount);
            try
            {
                BeginInvoke(new passDoubleUpdate(this.updateProgressBar), new object[] { Convert.ToInt16(currentTest.getPercentageComplete()) });
            }
            catch{}

            panelLeft.BackColor = Color.Gray;
            System.Threading.Thread.Sleep(300);
            panelLeft.BackColor = Color.Orange;

            if (currentSondeType.optionCable)
            {
                if (incoming.statusCable)
                {
                    panelAuxCable.BackColor = Color.LightGreen;
                }
                else
                {
                    panelAuxCable.BackColor = Color.Red;
                }
            }

        }

        void testComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            bool incoming = (bool)data.NewValue;

            currentTest.testError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(testError_PropertyChange);
            currentTest.testComplete.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(testComplete_PropertyChange);
            currentTest.updatePacket.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(updatePacket_PropertyChange);

            Program.programLog("Test Complete: " + incoming.ToString());

            FinalTest.testResults[] curTestResults = currentTest.getTestDataResults();
            FinalTest.testDataPoint curTestDataPoint = currentTest.getLatestDataPoint();
            
            //Building a local temp id data packet.
            LogClasses.idData tempID = new LogClasses.idData();
            tempID.currentUser = labelCurrentOperator.Text;
            tempID.currentWorkOrder = labelCurrentWorkOrder.Text;
            
            //Checking to make sure that a SN is entered before processing output.

            if (textBoxCurrentRadiosondeSerialNumber.Text == "")
            {
                MessageBox.Show("Serial Number has been entered.\n Please Enter in the current Serial Number then click this OK.", "SN Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            tempID.sondeSerialNumber = textBoxCurrentRadiosondeSerialNumber.Text;
            tempID.sondeTrackingNumber = textBoxCurrentRadiosondeTrackingNumber.Text;
            tempID.sondeTUNumber = textBoxCurrentRadiosondeTUNumber.Text;
            tempID.sondeFirmware = textBoxCurrentRadiosondeFirmware.Text;
            tempID.sondeType = curSondeType;

            if (incoming)   //If the test has passed.
            {
                panelLeft.BackColor = Color.Green;

                if(!labelCurrentWorkOrder.Text.ToLower().Contains("noprint"))
                {
                    Program.radiosondeSNLabel.printSNLabel(textBoxCurrentRadiosondeSerialNumber.Text);
                }

                int current = Convert.ToInt16(labelPassCounter.Text);
                current++;
                BeginInvoke(new UpdateButton(this.updatePassCounter), new object[] { current.ToString() });

                System.Media.SoundPlayer alertSound = new System.Media.SoundPlayer(AppDomain.CurrentDomain.BaseDirectory + @"Audio\ding.wav");
                alertSound.Play();
            }
            else
            {
                panelLeft.BackColor = Color.Red;

                System.Media.SoundPlayer alertSound = new System.Media.SoundPlayer(AppDomain.CurrentDomain.BaseDirectory + @"Audio\xpbatcrt.wav");
                alertSound.Play();
            }

            BeginInvoke(new passBoolUpdate(this.updateDisplayProgressBar), new object[] { false });

            BeginInvoke(new UpdateButton(this.updateButtonDisplay), new object[] { "Start Test" });  //Resetting button

            //Logging test results.

            if (!labelCurrentWorkOrder.Text.ToLower().Contains("nolog"))
            {
                LogClasses.logFinalTestResults currentLog = null;
                LogClasses.logFinalTestResults currentGlobalLog = null;

                try
                {
                    currentLog = new LogClasses.logFinalTestResults(Program.currentWOLog);
                    currentLog.logError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(logError_PropertyChange);
                    currentLog.appendLogData(curTestResults.Last(), curTestDataPoint, tempID);  //Sending out the data to be logged.
                }
                catch(Exception logError)
                {
                    Program.programLog("Final Test Log\n " + logError.Message);
                }

                try
                {
                    currentGlobalLog = new LogClasses.logFinalTestResults(Program.currentProgramSettings.masterLog);
                    currentGlobalLog.logError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(logError_PropertyChange);
                    currentGlobalLog.appendLogData(curTestResults.Last(), curTestDataPoint, tempID);  //Sending out the data to be logged.
                }
                
                catch(Exception logError)
                {
                    Program.programLog(logError.Message);
                }

                currentLog.logError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(logError_PropertyChange);
                currentGlobalLog.logError.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(logError_PropertyChange);
                
            }
            

            currentTest = null;     //Clearing the object data.

        }

        void logError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            Exception incoming = (Exception)data.NewValue;
            MessageBox.Show(incoming.Message + "\nPlease make sure logs are closed and retest radiosonde.","Log Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
        }

        #endregion

        #region Methods related to differances.

        private void updateDifferance()
        {
            double sondePressure = 0;
            double sondeAirTemp = 0;
            double sondeHumidity = 0;
            double refPressure = 0;
            double refAirTemp = 0;
            double refHumidity = 0;

            switch (this.currentSondeType.decoder.ToLower())
            {
                case "imet1":
                    iMet_1_Decoder.DecoderData tempData = Program.decoderIMet1.getCurrentDecoderData();
                    sondePressure = tempData.pressure;
                    sondeAirTemp = tempData.temperature;
                    sondeHumidity = tempData.humidity;
                    break;

                case "bell202":
                    txDecoder.radiosondePTUData tempPTU = Program.decoderBell202.getLatestPTU();
                    sondePressure = tempPTU.pressure;
                    sondeAirTemp = tempPTU.airTemp;
                    sondeHumidity = tempPTU.humidity;
                    break;
            }

            if (Program.sensorEE31 != null)     //Getting sensor data from the e+e sensor.
            {
                refAirTemp = Program.sensorEE31.currentAirTemp;
                refHumidity = Program.sensorEE31.currentHumidity;
            }

            if (Program.sensorHMP != null)      //Getting the sensor data from attached HMP234
            {
                refAirTemp = Program.sensorHMP.CurrentTemp;
                refHumidity = Program.sensorHMP.CurrentHumidity;
            }

            refPressure = Program.sensorBaro.CurrentPressure;   //Current Pressure system.


            double diffPressure = 0;
            double diffAirTemp = 0;
            double diffHumidity = 0;

            if (currentSondeType.optionPressure)
            {
                diffPressure = Math.Abs(sondePressure - refPressure);
            }

            diffAirTemp = Math.Abs(sondeAirTemp - refAirTemp);
            diffHumidity = Math.Abs(sondeHumidity - refHumidity);

            BeginInvoke(new UpdateDifferance(this.updateDifferanceDisplay), new object[] { diffPressure, diffAirTemp, diffHumidity });

        }

        #endregion

        #region Methods for inputing data into the system

        private void labelCurrentOperator_DoubleClick(object sender, EventArgs e)
        {
            string user = "";
            if (DialogResult.OK == Bill.Interfaces.InputBox("User Input", "Input Username:", ref user))
            {
                labelCurrentOperator.Text = user;
            }
        }

        private void labelCurrentWorkOrder_DoubleClick(object sender, EventArgs e)
        {
            string workOrder = "";
            if (DialogResult.OK == Bill.Interfaces.InputBox("User WO", "Input WO:", ref workOrder))
            {
                labelCurrentWorkOrder.Text = workOrder;
                Program.currentWOLog = Program.currentProgramSettings.baseDirWOLog + "\\" + workOrder + ".csv";
                labelCurrentLogFile.Text = Program.currentProgramSettings.baseDirWOLog + "\\" + workOrder + ".csv";
            }
        }

        private void autoTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (autoTestToolStripMenuItem.Checked)
            {
                autoTestToolStripMenuItem.Checked = false;
            }
            else
            {
                autoTestToolStripMenuItem.Checked = true;
            }
        }

        private void autoCableTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (autoCableTestToolStripMenuItem.Checked)
            {
                autoCableTestToolStripMenuItem.Checked = false;
            }
            else
            {
                autoCableTestToolStripMenuItem.Checked = true;
            }
        }

        private void testPrintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input = "";
            if (Bill.Interfaces.InputBox("Printer Test", "Input Number:", ref input) == DialogResult.OK)
            {
                Program.radiosondeSNLabel.printSNLabel(input);
            }
            
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Program.editSettings();
        }


        /// <summary>
        /// Methods for resizing to fit the resizing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMain_Resize(object sender, EventArgs e)
        {
            Form current = Form.ActiveForm;     //Getting the active form.

            if (current == null)
            {
                System.Diagnostics.Debug.WriteLine("Error: unable to resize");
                return;
            }

            int width = current.Size.Width;
            width = width / 2;
            width = width - (panelMainDisplay.Size.Width / 2);

            if (width < 57) { width = 57; }

            int height = current.Size.Height;
            height = height / 2;
            height = height - (panelMainDisplay.Size.Height / 2)-13;

            if (height < 27) { height = 27; }

            Point newLoc = new Point(width, height);   //Creating the new point.
            panelMainDisplay.Location = newLoc;     //Moving the main display to the new form center.

            //Moving and resizing the left sidee
            Size leftSize;
            if (current.Size.Width < 400)
            {
                leftSize = new Size(800, 600);
            }
            else
            {
                leftSize = new Size(current.Size.Width, current.Size.Height);
            }
            panelLeft.Size = leftSize;
            
        }


        #endregion

        private void buttonReset_Click(object sender, EventArgs e)
        {
            labelPassCounter.Text = "0";
        }

        private void buttonLoadWorkOrder_Click(object sender, EventArgs e)
        {
            OpenFileDialog openWorkOrder = new OpenFileDialog();
            openWorkOrder.Title = "Load Work Order";
            openWorkOrder.InitialDirectory = Program.currentProgramSettings.baseDirWOLog;

            if (DialogResult.OK == openWorkOrder.ShowDialog())
            {
                //Displaying the file selected.
                labelCurrentLogFile.Text = openWorkOrder.FileName;
                string[] fileBreakDown = openWorkOrder.FileName.Split('\\');
                labelCurrentWorkOrder.Text = fileBreakDown[fileBreakDown.Length - 1];
                Program.currentWOLog = openWorkOrder.FileName;
            }
        }


    }

    public class configRadiosonde
    {
        public string name;
        public bool optionGPS;
        public bool optionPressure;
        public bool optionCable;
        public string modeTX;
        public double testFrequancy;
        public string decoder;
    }

}
