﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FinalCheck3
{
    public partial class frmDebug : Form
    {
        delegate void UpdateText(string newText);

        public frmDebug()
        {
            InitializeComponent();
        }

        private void frmDebug_Load(object sender, EventArgs e)
        {
            Program.debugUpdate.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(debugUpdate_PropertyChange);
        }

        void debugUpdate_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            //BeginInvoke(new UpdateButton(this.updateRadiosondeIDDisplay), new object[] { sondeSN })
            try
            {
                BeginInvoke(new UpdateText(this.addLine), new object[] { data.NewValue });
            }
            catch
            {
            }
        }

        void addLine(string line)
        {
            textBoxDebug.AppendText(line + "\n");
        }

        private void frmDebug_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.debugUpdate.PropertyChange -= new updateCreater.objectUpdate.PropertyChangeHandler(debugUpdate_PropertyChange);
        }
    }
}
