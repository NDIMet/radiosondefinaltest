﻿namespace FinalCheck3
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.groupBoxTestParameters = new System.Windows.Forms.GroupBox();
            this.textBoxGPSVerTol = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxGPSHorzTol = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxHumidityTol = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.textBoxTempTol = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBoxPressureTol = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.buttonWODir = new System.Windows.Forms.Button();
            this.textBoxWODir = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonXCalDirectory = new System.Windows.Forms.Button();
            this.textBoxXCalDirectory = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonMasterLog = new System.Windows.Forms.Button();
            this.textBoxMasterLog = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxShippingPrinter = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxSNPrinter = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxMFGCable = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxAuxCable = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxBaro = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxHMP234 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxEE31 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxIMSDecoder = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxBell202 = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxProgramSettings = new System.Windows.Forms.GroupBox();
            this.groupBoxSensorBias = new System.Windows.Forms.GroupBox();
            this.textBoxStabilityU = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxStabilityT = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxStabilityP = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxBiasHumidity = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxBiasTemp = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxBiasP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.groupBoxTestParameters.SuspendLayout();
            this.groupBoxProgramSettings.SuspendLayout();
            this.groupBoxSensorBias.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxTestParameters
            // 
            this.groupBoxTestParameters.Controls.Add(this.textBoxGPSVerTol);
            this.groupBoxTestParameters.Controls.Add(this.label19);
            this.groupBoxTestParameters.Controls.Add(this.textBoxGPSHorzTol);
            this.groupBoxTestParameters.Controls.Add(this.label20);
            this.groupBoxTestParameters.Controls.Add(this.textBoxHumidityTol);
            this.groupBoxTestParameters.Controls.Add(this.label21);
            this.groupBoxTestParameters.Controls.Add(this.textBoxTempTol);
            this.groupBoxTestParameters.Controls.Add(this.label22);
            this.groupBoxTestParameters.Controls.Add(this.textBoxPressureTol);
            this.groupBoxTestParameters.Controls.Add(this.label23);
            this.groupBoxTestParameters.Location = new System.Drawing.Point(449, 12);
            this.groupBoxTestParameters.Name = "groupBoxTestParameters";
            this.groupBoxTestParameters.Size = new System.Drawing.Size(237, 184);
            this.groupBoxTestParameters.TabIndex = 0;
            this.groupBoxTestParameters.TabStop = false;
            this.groupBoxTestParameters.Text = "Radiosonde Test Parameters";
            // 
            // textBoxGPSVerTol
            // 
            this.textBoxGPSVerTol.Location = new System.Drawing.Point(111, 138);
            this.textBoxGPSVerTol.Name = "textBoxGPSVerTol";
            this.textBoxGPSVerTol.Size = new System.Drawing.Size(100, 20);
            this.textBoxGPSVerTol.TabIndex = 19;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 141);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(88, 13);
            this.label19.TabIndex = 18;
            this.label19.Text = "GPS Vertical Tol:";
            // 
            // textBoxGPSHorzTol
            // 
            this.textBoxGPSHorzTol.Location = new System.Drawing.Point(111, 112);
            this.textBoxGPSHorzTol.Name = "textBoxGPSHorzTol";
            this.textBoxGPSHorzTol.Size = new System.Drawing.Size(100, 20);
            this.textBoxGPSHorzTol.TabIndex = 17;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(8, 115);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "GPS Horizonal Tol:";
            // 
            // textBoxHumidityTol
            // 
            this.textBoxHumidityTol.Location = new System.Drawing.Point(111, 86);
            this.textBoxHumidityTol.Name = "textBoxHumidityTol";
            this.textBoxHumidityTol.Size = new System.Drawing.Size(100, 20);
            this.textBoxHumidityTol.TabIndex = 15;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(37, 89);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 13);
            this.label21.TabIndex = 14;
            this.label21.Text = "Humidity Tol:";
            // 
            // textBoxTempTol
            // 
            this.textBoxTempTol.Location = new System.Drawing.Point(111, 60);
            this.textBoxTempTol.Name = "textBoxTempTol";
            this.textBoxTempTol.Size = new System.Drawing.Size(100, 20);
            this.textBoxTempTol.TabIndex = 13;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(50, 63);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "Temp Tol:";
            // 
            // textBoxPressureTol
            // 
            this.textBoxPressureTol.Location = new System.Drawing.Point(111, 34);
            this.textBoxPressureTol.Name = "textBoxPressureTol";
            this.textBoxPressureTol.Size = new System.Drawing.Size(100, 20);
            this.textBoxPressureTol.TabIndex = 11;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(36, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(69, 13);
            this.label23.TabIndex = 10;
            this.label23.Text = "Pressure Tol:";
            // 
            // buttonWODir
            // 
            this.buttonWODir.Location = new System.Drawing.Point(401, 358);
            this.buttonWODir.Name = "buttonWODir";
            this.buttonWODir.Size = new System.Drawing.Size(22, 24);
            this.buttonWODir.TabIndex = 26;
            this.buttonWODir.Text = "...";
            this.buttonWODir.UseVisualStyleBackColor = true;
            this.buttonWODir.Click += new System.EventHandler(this.buttonWODir_Click);
            // 
            // textBoxWODir
            // 
            this.textBoxWODir.Location = new System.Drawing.Point(10, 362);
            this.textBoxWODir.Name = "textBoxWODir";
            this.textBoxWODir.Size = new System.Drawing.Size(385, 20);
            this.textBoxWODir.TabIndex = 25;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 346);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "WO Dir";
            // 
            // buttonXCalDirectory
            // 
            this.buttonXCalDirectory.Location = new System.Drawing.Point(401, 316);
            this.buttonXCalDirectory.Name = "buttonXCalDirectory";
            this.buttonXCalDirectory.Size = new System.Drawing.Size(22, 24);
            this.buttonXCalDirectory.TabIndex = 23;
            this.buttonXCalDirectory.Text = "...";
            this.buttonXCalDirectory.UseVisualStyleBackColor = true;
            this.buttonXCalDirectory.Click += new System.EventHandler(this.buttonXCalDirectory_Click);
            // 
            // textBoxXCalDirectory
            // 
            this.textBoxXCalDirectory.Location = new System.Drawing.Point(10, 320);
            this.textBoxXCalDirectory.Name = "textBoxXCalDirectory";
            this.textBoxXCalDirectory.Size = new System.Drawing.Size(385, 20);
            this.textBoxXCalDirectory.TabIndex = 22;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 304);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 13);
            this.label12.TabIndex = 21;
            this.label12.Text = "XCal Directory";
            // 
            // buttonMasterLog
            // 
            this.buttonMasterLog.Location = new System.Drawing.Point(401, 274);
            this.buttonMasterLog.Name = "buttonMasterLog";
            this.buttonMasterLog.Size = new System.Drawing.Size(22, 24);
            this.buttonMasterLog.TabIndex = 20;
            this.buttonMasterLog.Text = "...";
            this.buttonMasterLog.UseVisualStyleBackColor = true;
            this.buttonMasterLog.Click += new System.EventHandler(this.buttonMasterLog_Click);
            // 
            // textBoxMasterLog
            // 
            this.textBoxMasterLog.Location = new System.Drawing.Point(10, 278);
            this.textBoxMasterLog.Name = "textBoxMasterLog";
            this.textBoxMasterLog.Size = new System.Drawing.Size(385, 20);
            this.textBoxMasterLog.TabIndex = 19;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 262);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Master Log";
            // 
            // comboBoxShippingPrinter
            // 
            this.comboBoxShippingPrinter.FormattingEnabled = true;
            this.comboBoxShippingPrinter.Location = new System.Drawing.Point(84, 237);
            this.comboBoxShippingPrinter.Name = "comboBoxShippingPrinter";
            this.comboBoxShippingPrinter.Size = new System.Drawing.Size(339, 21);
            this.comboBoxShippingPrinter.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Shipping Printer";
            // 
            // comboBoxSNPrinter
            // 
            this.comboBoxSNPrinter.FormattingEnabled = true;
            this.comboBoxSNPrinter.Location = new System.Drawing.Point(85, 211);
            this.comboBoxSNPrinter.Name = "comboBoxSNPrinter";
            this.comboBoxSNPrinter.Size = new System.Drawing.Size(339, 21);
            this.comboBoxSNPrinter.TabIndex = 15;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "SN Printer";
            // 
            // comboBoxMFGCable
            // 
            this.comboBoxMFGCable.FormattingEnabled = true;
            this.comboBoxMFGCable.Location = new System.Drawing.Point(85, 184);
            this.comboBoxMFGCable.Name = "comboBoxMFGCable";
            this.comboBoxMFGCable.Size = new System.Drawing.Size(72, 21);
            this.comboBoxMFGCable.TabIndex = 13;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 187);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "MFG Cable";
            // 
            // comboBoxAuxCable
            // 
            this.comboBoxAuxCable.FormattingEnabled = true;
            this.comboBoxAuxCable.Location = new System.Drawing.Point(85, 157);
            this.comboBoxAuxCable.Name = "comboBoxAuxCable";
            this.comboBoxAuxCable.Size = new System.Drawing.Size(72, 21);
            this.comboBoxAuxCable.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 160);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Aux Cable";
            // 
            // comboBoxBaro
            // 
            this.comboBoxBaro.FormattingEnabled = true;
            this.comboBoxBaro.Location = new System.Drawing.Point(85, 130);
            this.comboBoxBaro.Name = "comboBoxBaro";
            this.comboBoxBaro.Size = new System.Drawing.Size(72, 21);
            this.comboBoxBaro.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Baro";
            // 
            // comboBoxHMP234
            // 
            this.comboBoxHMP234.FormattingEnabled = true;
            this.comboBoxHMP234.Location = new System.Drawing.Point(85, 103);
            this.comboBoxHMP234.Name = "comboBoxHMP234";
            this.comboBoxHMP234.Size = new System.Drawing.Size(72, 21);
            this.comboBoxHMP234.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 106);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "HMP234";
            // 
            // comboBoxEE31
            // 
            this.comboBoxEE31.FormattingEnabled = true;
            this.comboBoxEE31.Location = new System.Drawing.Point(85, 76);
            this.comboBoxEE31.Name = "comboBoxEE31";
            this.comboBoxEE31.Size = new System.Drawing.Size(72, 21);
            this.comboBoxEE31.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "E + E";
            // 
            // comboBoxIMSDecoder
            // 
            this.comboBoxIMSDecoder.FormattingEnabled = true;
            this.comboBoxIMSDecoder.Location = new System.Drawing.Point(85, 49);
            this.comboBoxIMSDecoder.Name = "comboBoxIMSDecoder";
            this.comboBoxIMSDecoder.Size = new System.Drawing.Size(72, 21);
            this.comboBoxIMSDecoder.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "IMet1";
            // 
            // comboBoxBell202
            // 
            this.comboBoxBell202.FormattingEnabled = true;
            this.comboBoxBell202.Location = new System.Drawing.Point(85, 23);
            this.comboBoxBell202.Name = "comboBoxBell202";
            this.comboBoxBell202.Size = new System.Drawing.Size(72, 21);
            this.comboBoxBell202.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Bell202";
            // 
            // groupBoxProgramSettings
            // 
            this.groupBoxProgramSettings.Controls.Add(this.groupBoxSensorBias);
            this.groupBoxProgramSettings.Controls.Add(this.buttonWODir);
            this.groupBoxProgramSettings.Controls.Add(this.label2);
            this.groupBoxProgramSettings.Controls.Add(this.textBoxWODir);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxBell202);
            this.groupBoxProgramSettings.Controls.Add(this.label13);
            this.groupBoxProgramSettings.Controls.Add(this.label3);
            this.groupBoxProgramSettings.Controls.Add(this.buttonXCalDirectory);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxIMSDecoder);
            this.groupBoxProgramSettings.Controls.Add(this.textBoxXCalDirectory);
            this.groupBoxProgramSettings.Controls.Add(this.label4);
            this.groupBoxProgramSettings.Controls.Add(this.label12);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxEE31);
            this.groupBoxProgramSettings.Controls.Add(this.buttonMasterLog);
            this.groupBoxProgramSettings.Controls.Add(this.label5);
            this.groupBoxProgramSettings.Controls.Add(this.textBoxMasterLog);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxHMP234);
            this.groupBoxProgramSettings.Controls.Add(this.label11);
            this.groupBoxProgramSettings.Controls.Add(this.label6);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxShippingPrinter);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxBaro);
            this.groupBoxProgramSettings.Controls.Add(this.label10);
            this.groupBoxProgramSettings.Controls.Add(this.label7);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxSNPrinter);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxAuxCable);
            this.groupBoxProgramSettings.Controls.Add(this.label9);
            this.groupBoxProgramSettings.Controls.Add(this.label8);
            this.groupBoxProgramSettings.Controls.Add(this.comboBoxMFGCable);
            this.groupBoxProgramSettings.Location = new System.Drawing.Point(12, 12);
            this.groupBoxProgramSettings.Name = "groupBoxProgramSettings";
            this.groupBoxProgramSettings.Size = new System.Drawing.Size(431, 394);
            this.groupBoxProgramSettings.TabIndex = 1;
            this.groupBoxProgramSettings.TabStop = false;
            this.groupBoxProgramSettings.Text = "Program Settings";
            // 
            // groupBoxSensorBias
            // 
            this.groupBoxSensorBias.Controls.Add(this.textBoxStabilityU);
            this.groupBoxSensorBias.Controls.Add(this.label18);
            this.groupBoxSensorBias.Controls.Add(this.textBoxStabilityT);
            this.groupBoxSensorBias.Controls.Add(this.label17);
            this.groupBoxSensorBias.Controls.Add(this.textBoxStabilityP);
            this.groupBoxSensorBias.Controls.Add(this.label16);
            this.groupBoxSensorBias.Controls.Add(this.textBoxBiasHumidity);
            this.groupBoxSensorBias.Controls.Add(this.label15);
            this.groupBoxSensorBias.Controls.Add(this.textBoxBiasTemp);
            this.groupBoxSensorBias.Controls.Add(this.label14);
            this.groupBoxSensorBias.Controls.Add(this.textBoxBiasP);
            this.groupBoxSensorBias.Controls.Add(this.label1);
            this.groupBoxSensorBias.Location = new System.Drawing.Point(291, 19);
            this.groupBoxSensorBias.Name = "groupBoxSensorBias";
            this.groupBoxSensorBias.Size = new System.Drawing.Size(134, 181);
            this.groupBoxSensorBias.TabIndex = 27;
            this.groupBoxSensorBias.TabStop = false;
            this.groupBoxSensorBias.Text = "Sensor Bias";
            // 
            // textBoxStabilityU
            // 
            this.textBoxStabilityU.Location = new System.Drawing.Point(86, 145);
            this.textBoxStabilityU.Name = "textBoxStabilityU";
            this.textBoxStabilityU.Size = new System.Drawing.Size(43, 20);
            this.textBoxStabilityU.TabIndex = 11;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(24, 148);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "Stability U:";
            // 
            // textBoxStabilityT
            // 
            this.textBoxStabilityT.Location = new System.Drawing.Point(86, 119);
            this.textBoxStabilityT.Name = "textBoxStabilityT";
            this.textBoxStabilityT.Size = new System.Drawing.Size(43, 20);
            this.textBoxStabilityT.TabIndex = 9;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 122);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(56, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Stability T:";
            // 
            // textBoxStabilityP
            // 
            this.textBoxStabilityP.Location = new System.Drawing.Point(86, 93);
            this.textBoxStabilityP.Name = "textBoxStabilityP";
            this.textBoxStabilityP.Size = new System.Drawing.Size(43, 20);
            this.textBoxStabilityP.TabIndex = 7;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(24, 96);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Stability P:";
            // 
            // textBoxBiasHumidity
            // 
            this.textBoxBiasHumidity.Location = new System.Drawing.Point(86, 67);
            this.textBoxBiasHumidity.Name = "textBoxBiasHumidity";
            this.textBoxBiasHumidity.Size = new System.Drawing.Size(43, 20);
            this.textBoxBiasHumidity.TabIndex = 5;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 70);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "Humidity Bias:";
            // 
            // textBoxBiasTemp
            // 
            this.textBoxBiasTemp.Location = new System.Drawing.Point(86, 41);
            this.textBoxBiasTemp.Name = "textBoxBiasTemp";
            this.textBoxBiasTemp.Size = new System.Drawing.Size(43, 20);
            this.textBoxBiasTemp.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(20, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Temp Bias:";
            // 
            // textBoxBiasP
            // 
            this.textBoxBiasP.Location = new System.Drawing.Point(86, 15);
            this.textBoxBiasP.Name = "textBoxBiasP";
            this.textBoxBiasP.Size = new System.Drawing.Size(43, 20);
            this.textBoxBiasP.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pressure Bias:";
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(274, 412);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "Apply";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(436, 412);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(355, 412);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 4;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // frmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 442);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.groupBoxProgramSettings);
            this.Controls.Add(this.groupBoxTestParameters);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSettings";
            this.Text = "Settings";
            this.groupBoxTestParameters.ResumeLayout(false);
            this.groupBoxTestParameters.PerformLayout();
            this.groupBoxProgramSettings.ResumeLayout(false);
            this.groupBoxProgramSettings.PerformLayout();
            this.groupBoxSensorBias.ResumeLayout(false);
            this.groupBoxSensorBias.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxTestParameters;
        private System.Windows.Forms.GroupBox groupBoxProgramSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxBell202;
        private System.Windows.Forms.ComboBox comboBoxIMSDecoder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxEE31;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxBaro;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxHMP234;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxMFGCable;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxAuxCable;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxShippingPrinter;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxSNPrinter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonMasterLog;
        private System.Windows.Forms.TextBox textBoxMasterLog;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonXCalDirectory;
        private System.Windows.Forms.TextBox textBoxXCalDirectory;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonWODir;
        private System.Windows.Forms.TextBox textBoxWODir;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBoxSensorBias;
        private System.Windows.Forms.TextBox textBoxBiasHumidity;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxBiasTemp;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxBiasP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxStabilityU;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxStabilityT;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxStabilityP;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxGPSVerTol;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxGPSHorzTol;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxHumidityTol;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox textBoxTempTol;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBoxPressureTol;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button buttonClose;

    }
}