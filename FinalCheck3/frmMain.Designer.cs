﻿namespace FinalCheck3
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.panelMainDisplay = new System.Windows.Forms.Panel();
            this.groupBoxPassCount = new System.Windows.Forms.GroupBox();
            this.buttonReset = new System.Windows.Forms.Button();
            this.labelPassCounter = new System.Windows.Forms.Label();
            this.groupBoxAuxCableTest = new System.Windows.Forms.GroupBox();
            this.panelAuxCable = new System.Windows.Forms.Panel();
            this.labelCableTestResults = new System.Windows.Forms.Label();
            this.labelCableTestStatus = new System.Windows.Forms.Label();
            this.buttonTestAuxCable = new System.Windows.Forms.Button();
            this.groupBoxSensorStability = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxSondePressureChange = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxRefPressureChange = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxSondeTempChange = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxRefHumidityChange = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxRefTempChange = new System.Windows.Forms.TextBox();
            this.textBoxSondeHumidityChange = new System.Windows.Forms.TextBox();
            this.groupBoxRadiosondeInformation = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeFirmware = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.buttonStartTest = new System.Windows.Forms.Button();
            this.comboBoxRadiosondeType = new System.Windows.Forms.ComboBox();
            this.textBoxCurrentRadiosondeTUNumber = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeTrackingNumber = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeSerialNumber = new System.Windows.Forms.TextBox();
            this.groupBoxAmbientValues = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxCurrentSensorRHDiff = new System.Windows.Forms.TextBox();
            this.textBoxCurrentSensorPressureDiff = new System.Windows.Forms.TextBox();
            this.textBoxCurrentSensorAirTempDiff = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxCurrentSensorRH = new System.Windows.Forms.TextBox();
            this.textBoxCurrentSensorPressure = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxCurrentSensorAirTemp = new System.Windows.Forms.TextBox();
            this.groupBoxRadiosondePTUandGPS = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeAlt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeLong = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeLat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeRH = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondeAirTemp = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxCurrentRadiosondePressure = new System.Windows.Forms.TextBox();
            this.groupBoxRadiosondeFinalTest = new System.Windows.Forms.GroupBox();
            this.buttonLoadWorkOrder = new System.Windows.Forms.Button();
            this.labelCurrentLogFile = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.labelCurrentOperator = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.labelCurrentWorkOrder = new System.Windows.Forms.Label();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testPrintToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoCableTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelSondeBattery = new System.Windows.Forms.ToolStripStatusLabel();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelMainDisplay.SuspendLayout();
            this.groupBoxPassCount.SuspendLayout();
            this.groupBoxAuxCableTest.SuspendLayout();
            this.panelAuxCable.SuspendLayout();
            this.groupBoxSensorStability.SuspendLayout();
            this.groupBoxRadiosondeInformation.SuspendLayout();
            this.groupBoxAmbientValues.SuspendLayout();
            this.groupBoxRadiosondePTUandGPS.SuspendLayout();
            this.groupBoxRadiosondeFinalTest.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            this.statusStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMainDisplay
            // 
            this.panelMainDisplay.Controls.Add(this.groupBoxPassCount);
            this.panelMainDisplay.Controls.Add(this.groupBoxAuxCableTest);
            this.panelMainDisplay.Controls.Add(this.groupBoxSensorStability);
            this.panelMainDisplay.Controls.Add(this.groupBoxRadiosondeInformation);
            this.panelMainDisplay.Controls.Add(this.groupBoxAmbientValues);
            this.panelMainDisplay.Controls.Add(this.groupBoxRadiosondePTUandGPS);
            this.panelMainDisplay.Controls.Add(this.groupBoxRadiosondeFinalTest);
            this.panelMainDisplay.Location = new System.Drawing.Point(57, 27);
            this.panelMainDisplay.Name = "panelMainDisplay";
            this.panelMainDisplay.Size = new System.Drawing.Size(660, 515);
            this.panelMainDisplay.TabIndex = 0;
            // 
            // groupBoxPassCount
            // 
            this.groupBoxPassCount.Controls.Add(this.buttonReset);
            this.groupBoxPassCount.Controls.Add(this.labelPassCounter);
            this.groupBoxPassCount.Location = new System.Drawing.Point(4, 367);
            this.groupBoxPassCount.Name = "groupBoxPassCount";
            this.groupBoxPassCount.Size = new System.Drawing.Size(145, 78);
            this.groupBoxPassCount.TabIndex = 12;
            this.groupBoxPassCount.TabStop = false;
            this.groupBoxPassCount.Text = "Pass Count";
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(67, 22);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 1;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // labelPassCounter
            // 
            this.labelPassCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassCounter.Location = new System.Drawing.Point(6, 16);
            this.labelPassCounter.Name = "labelPassCounter";
            this.labelPassCounter.Size = new System.Drawing.Size(55, 29);
            this.labelPassCounter.TabIndex = 0;
            this.labelPassCounter.Text = "0";
            this.labelPassCounter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBoxAuxCableTest
            // 
            this.groupBoxAuxCableTest.Controls.Add(this.panelAuxCable);
            this.groupBoxAuxCableTest.Controls.Add(this.buttonTestAuxCable);
            this.groupBoxAuxCableTest.Location = new System.Drawing.Point(155, 370);
            this.groupBoxAuxCableTest.Name = "groupBoxAuxCableTest";
            this.groupBoxAuxCableTest.Size = new System.Drawing.Size(274, 75);
            this.groupBoxAuxCableTest.TabIndex = 11;
            this.groupBoxAuxCableTest.TabStop = false;
            this.groupBoxAuxCableTest.Text = "Aux Cable Test";
            // 
            // panelAuxCable
            // 
            this.panelAuxCable.Controls.Add(this.labelCableTestResults);
            this.panelAuxCable.Controls.Add(this.labelCableTestStatus);
            this.panelAuxCable.Location = new System.Drawing.Point(107, 8);
            this.panelAuxCable.Name = "panelAuxCable";
            this.panelAuxCable.Size = new System.Drawing.Size(165, 67);
            this.panelAuxCable.TabIndex = 1;
            this.panelAuxCable.Visible = false;
            // 
            // labelCableTestResults
            // 
            this.labelCableTestResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCableTestResults.Location = new System.Drawing.Point(6, 35);
            this.labelCableTestResults.Name = "labelCableTestResults";
            this.labelCableTestResults.Size = new System.Drawing.Size(152, 20);
            this.labelCableTestResults.TabIndex = 1;
            this.labelCableTestResults.Text = "Cable Test Results";
            this.labelCableTestResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelCableTestStatus
            // 
            this.labelCableTestStatus.Location = new System.Drawing.Point(8, 7);
            this.labelCableTestStatus.Name = "labelCableTestStatus";
            this.labelCableTestStatus.Size = new System.Drawing.Size(152, 17);
            this.labelCableTestStatus.TabIndex = 0;
            this.labelCableTestStatus.Text = "Cable Test Status";
            this.labelCableTestStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonTestAuxCable
            // 
            this.buttonTestAuxCable.Location = new System.Drawing.Point(6, 32);
            this.buttonTestAuxCable.Name = "buttonTestAuxCable";
            this.buttonTestAuxCable.Size = new System.Drawing.Size(96, 23);
            this.buttonTestAuxCable.TabIndex = 0;
            this.buttonTestAuxCable.Text = "Test Aux Cable";
            this.buttonTestAuxCable.UseVisualStyleBackColor = true;
            // 
            // groupBoxSensorStability
            // 
            this.groupBoxSensorStability.Controls.Add(this.label18);
            this.groupBoxSensorStability.Controls.Add(this.textBoxSondePressureChange);
            this.groupBoxSensorStability.Controls.Add(this.label19);
            this.groupBoxSensorStability.Controls.Add(this.textBoxRefPressureChange);
            this.groupBoxSensorStability.Controls.Add(this.label16);
            this.groupBoxSensorStability.Controls.Add(this.textBoxSondeTempChange);
            this.groupBoxSensorStability.Controls.Add(this.label14);
            this.groupBoxSensorStability.Controls.Add(this.label17);
            this.groupBoxSensorStability.Controls.Add(this.textBoxRefHumidityChange);
            this.groupBoxSensorStability.Controls.Add(this.label15);
            this.groupBoxSensorStability.Controls.Add(this.textBoxRefTempChange);
            this.groupBoxSensorStability.Controls.Add(this.textBoxSondeHumidityChange);
            this.groupBoxSensorStability.Location = new System.Drawing.Point(435, 276);
            this.groupBoxSensorStability.Name = "groupBoxSensorStability";
            this.groupBoxSensorStability.Size = new System.Drawing.Size(219, 169);
            this.groupBoxSensorStability.TabIndex = 10;
            this.groupBoxSensorStability.TabStop = false;
            this.groupBoxSensorStability.Text = "Sensor Stability";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 41);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(133, 13);
            this.label18.TabIndex = 32;
            this.label18.Text = "Sonde P Change (+/- hPa)";
            // 
            // textBoxSondePressureChange
            // 
            this.textBoxSondePressureChange.Location = new System.Drawing.Point(146, 39);
            this.textBoxSondePressureChange.Name = "textBoxSondePressureChange";
            this.textBoxSondePressureChange.Size = new System.Drawing.Size(57, 20);
            this.textBoxSondePressureChange.TabIndex = 31;
            this.textBoxSondePressureChange.TabStop = false;
            this.textBoxSondePressureChange.Text = "0";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(21, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 13);
            this.label19.TabIndex = 30;
            this.label19.Text = "Ref P Change (+/- hPa)";
            // 
            // textBoxRefPressureChange
            // 
            this.textBoxRefPressureChange.Location = new System.Drawing.Point(146, 13);
            this.textBoxRefPressureChange.Name = "textBoxRefPressureChange";
            this.textBoxRefPressureChange.Size = new System.Drawing.Size(57, 20);
            this.textBoxRefPressureChange.TabIndex = 29;
            this.textBoxRefPressureChange.TabStop = false;
            this.textBoxRefPressureChange.Text = "0";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 94);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(125, 13);
            this.label16.TabIndex = 28;
            this.label16.Text = "Sonde AT Change (+/-C)";
            // 
            // textBoxSondeTempChange
            // 
            this.textBoxSondeTempChange.Location = new System.Drawing.Point(145, 91);
            this.textBoxSondeTempChange.Name = "textBoxSondeTempChange";
            this.textBoxSondeTempChange.Size = new System.Drawing.Size(57, 20);
            this.textBoxSondeTempChange.TabIndex = 27;
            this.textBoxSondeTempChange.TabStop = false;
            this.textBoxSondeTempChange.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(31, 120);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Ref U Change (%RH)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(28, 68);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 13);
            this.label17.TabIndex = 26;
            this.label17.Text = "Ref AT Change (+/-C)";
            // 
            // textBoxRefHumidityChange
            // 
            this.textBoxRefHumidityChange.Location = new System.Drawing.Point(145, 117);
            this.textBoxRefHumidityChange.Name = "textBoxRefHumidityChange";
            this.textBoxRefHumidityChange.Size = new System.Drawing.Size(57, 20);
            this.textBoxRefHumidityChange.TabIndex = 21;
            this.textBoxRefHumidityChange.TabStop = false;
            this.textBoxRefHumidityChange.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(17, 146);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 13);
            this.label15.TabIndex = 24;
            this.label15.Text = "Sonde U Change (%RH)";
            // 
            // textBoxRefTempChange
            // 
            this.textBoxRefTempChange.Location = new System.Drawing.Point(145, 65);
            this.textBoxRefTempChange.Name = "textBoxRefTempChange";
            this.textBoxRefTempChange.Size = new System.Drawing.Size(57, 20);
            this.textBoxRefTempChange.TabIndex = 25;
            this.textBoxRefTempChange.TabStop = false;
            this.textBoxRefTempChange.Text = "0";
            // 
            // textBoxSondeHumidityChange
            // 
            this.textBoxSondeHumidityChange.Location = new System.Drawing.Point(145, 143);
            this.textBoxSondeHumidityChange.Name = "textBoxSondeHumidityChange";
            this.textBoxSondeHumidityChange.Size = new System.Drawing.Size(57, 20);
            this.textBoxSondeHumidityChange.TabIndex = 23;
            this.textBoxSondeHumidityChange.TabStop = false;
            this.textBoxSondeHumidityChange.Text = "0";
            // 
            // groupBoxRadiosondeInformation
            // 
            this.groupBoxRadiosondeInformation.Controls.Add(this.label12);
            this.groupBoxRadiosondeInformation.Controls.Add(this.textBoxCurrentRadiosondeFirmware);
            this.groupBoxRadiosondeInformation.Controls.Add(this.label13);
            this.groupBoxRadiosondeInformation.Controls.Add(this.buttonStartTest);
            this.groupBoxRadiosondeInformation.Controls.Add(this.comboBoxRadiosondeType);
            this.groupBoxRadiosondeInformation.Controls.Add(this.textBoxCurrentRadiosondeTUNumber);
            this.groupBoxRadiosondeInformation.Controls.Add(this.label11);
            this.groupBoxRadiosondeInformation.Controls.Add(this.textBoxCurrentRadiosondeTrackingNumber);
            this.groupBoxRadiosondeInformation.Controls.Add(this.label10);
            this.groupBoxRadiosondeInformation.Controls.Add(this.textBoxCurrentRadiosondeSerialNumber);
            this.groupBoxRadiosondeInformation.Location = new System.Drawing.Point(3, 77);
            this.groupBoxRadiosondeInformation.Name = "groupBoxRadiosondeInformation";
            this.groupBoxRadiosondeInformation.Size = new System.Drawing.Size(651, 129);
            this.groupBoxRadiosondeInformation.TabIndex = 9;
            this.groupBoxRadiosondeInformation.TabStop = false;
            this.groupBoxRadiosondeInformation.Text = "Radiosonde Information";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(86, 61);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(115, 29);
            this.label12.TabIndex = 28;
            this.label12.Text = "Firmware";
            // 
            // textBoxCurrentRadiosondeFirmware
            // 
            this.textBoxCurrentRadiosondeFirmware.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCurrentRadiosondeFirmware.Location = new System.Drawing.Point(207, 58);
            this.textBoxCurrentRadiosondeFirmware.Name = "textBoxCurrentRadiosondeFirmware";
            this.textBoxCurrentRadiosondeFirmware.Size = new System.Drawing.Size(192, 35);
            this.textBoxCurrentRadiosondeFirmware.TabIndex = 27;
            this.textBoxCurrentRadiosondeFirmware.TabStop = false;
            this.textBoxCurrentRadiosondeFirmware.Text = "5.17";
            this.textBoxCurrentRadiosondeFirmware.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(241, 102);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "TU Number";
            // 
            // buttonStartTest
            // 
            this.buttonStartTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartTest.Location = new System.Drawing.Point(425, 64);
            this.buttonStartTest.Name = "buttonStartTest";
            this.buttonStartTest.Size = new System.Drawing.Size(193, 47);
            this.buttonStartTest.TabIndex = 7;
            this.buttonStartTest.Text = "Start Test";
            this.buttonStartTest.UseVisualStyleBackColor = true;
            this.buttonStartTest.Click += new System.EventHandler(this.buttonStartTest_Click);
            // 
            // comboBoxRadiosondeType
            // 
            this.comboBoxRadiosondeType.DropDownHeight = 300;
            this.comboBoxRadiosondeType.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxRadiosondeType.FormattingEnabled = true;
            this.comboBoxRadiosondeType.IntegralHeight = false;
            this.comboBoxRadiosondeType.Location = new System.Drawing.Point(405, 17);
            this.comboBoxRadiosondeType.Name = "comboBoxRadiosondeType";
            this.comboBoxRadiosondeType.Size = new System.Drawing.Size(237, 37);
            this.comboBoxRadiosondeType.TabIndex = 9;
            this.comboBoxRadiosondeType.Text = "Radiosonde Type";
            this.comboBoxRadiosondeType.SelectedIndexChanged += new System.EventHandler(this.comboBoxRadiosondeType_SelectedIndexChanged);
            this.comboBoxRadiosondeType.Click += new System.EventHandler(this.comboBoxRadiosondeType_Click);
            // 
            // textBoxCurrentRadiosondeTUNumber
            // 
            this.textBoxCurrentRadiosondeTUNumber.Location = new System.Drawing.Point(309, 99);
            this.textBoxCurrentRadiosondeTUNumber.Name = "textBoxCurrentRadiosondeTUNumber";
            this.textBoxCurrentRadiosondeTUNumber.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentRadiosondeTUNumber.TabIndex = 25;
            this.textBoxCurrentRadiosondeTUNumber.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(46, 102);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Tracking Number";
            // 
            // textBoxCurrentRadiosondeTrackingNumber
            // 
            this.textBoxCurrentRadiosondeTrackingNumber.Location = new System.Drawing.Point(141, 99);
            this.textBoxCurrentRadiosondeTrackingNumber.Name = "textBoxCurrentRadiosondeTrackingNumber";
            this.textBoxCurrentRadiosondeTrackingNumber.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentRadiosondeTrackingNumber.TabIndex = 23;
            this.textBoxCurrentRadiosondeTrackingNumber.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(32, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(169, 29);
            this.label10.TabIndex = 22;
            this.label10.Text = "Serial Number";
            // 
            // textBoxCurrentRadiosondeSerialNumber
            // 
            this.textBoxCurrentRadiosondeSerialNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCurrentRadiosondeSerialNumber.Location = new System.Drawing.Point(207, 17);
            this.textBoxCurrentRadiosondeSerialNumber.Name = "textBoxCurrentRadiosondeSerialNumber";
            this.textBoxCurrentRadiosondeSerialNumber.Size = new System.Drawing.Size(192, 35);
            this.textBoxCurrentRadiosondeSerialNumber.TabIndex = 21;
            this.textBoxCurrentRadiosondeSerialNumber.TabStop = false;
            this.textBoxCurrentRadiosondeSerialNumber.Text = "12345667";
            this.textBoxCurrentRadiosondeSerialNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBoxAmbientValues
            // 
            this.groupBoxAmbientValues.Controls.Add(this.label39);
            this.groupBoxAmbientValues.Controls.Add(this.label38);
            this.groupBoxAmbientValues.Controls.Add(this.textBoxCurrentSensorRHDiff);
            this.groupBoxAmbientValues.Controls.Add(this.textBoxCurrentSensorPressureDiff);
            this.groupBoxAmbientValues.Controls.Add(this.textBoxCurrentSensorAirTempDiff);
            this.groupBoxAmbientValues.Controls.Add(this.label7);
            this.groupBoxAmbientValues.Controls.Add(this.label9);
            this.groupBoxAmbientValues.Controls.Add(this.textBoxCurrentSensorRH);
            this.groupBoxAmbientValues.Controls.Add(this.textBoxCurrentSensorPressure);
            this.groupBoxAmbientValues.Controls.Add(this.label8);
            this.groupBoxAmbientValues.Controls.Add(this.textBoxCurrentSensorAirTemp);
            this.groupBoxAmbientValues.Location = new System.Drawing.Point(3, 276);
            this.groupBoxAmbientValues.Name = "groupBoxAmbientValues";
            this.groupBoxAmbientValues.Size = new System.Drawing.Size(426, 88);
            this.groupBoxAmbientValues.TabIndex = 8;
            this.groupBoxAmbientValues.TabStop = false;
            this.groupBoxAmbientValues.Text = "Ambient Values";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(297, 61);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(56, 13);
            this.label39.TabIndex = 22;
            this.label39.Text = "Differance";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(297, 35);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 13);
            this.label38.TabIndex = 21;
            this.label38.Text = "Current";
            // 
            // textBoxCurrentSensorRHDiff
            // 
            this.textBoxCurrentSensorRHDiff.Location = new System.Drawing.Point(204, 58);
            this.textBoxCurrentSensorRHDiff.Name = "textBoxCurrentSensorRHDiff";
            this.textBoxCurrentSensorRHDiff.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentSensorRHDiff.TabIndex = 20;
            this.textBoxCurrentSensorRHDiff.TabStop = false;
            this.textBoxCurrentSensorRHDiff.Text = "0";
            // 
            // textBoxCurrentSensorPressureDiff
            // 
            this.textBoxCurrentSensorPressureDiff.Location = new System.Drawing.Point(17, 58);
            this.textBoxCurrentSensorPressureDiff.Name = "textBoxCurrentSensorPressureDiff";
            this.textBoxCurrentSensorPressureDiff.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentSensorPressureDiff.TabIndex = 18;
            this.textBoxCurrentSensorPressureDiff.TabStop = false;
            this.textBoxCurrentSensorPressureDiff.Text = "0";
            // 
            // textBoxCurrentSensorAirTempDiff
            // 
            this.textBoxCurrentSensorAirTempDiff.Location = new System.Drawing.Point(116, 58);
            this.textBoxCurrentSensorAirTempDiff.Name = "textBoxCurrentSensorAirTempDiff";
            this.textBoxCurrentSensorAirTempDiff.Size = new System.Drawing.Size(79, 20);
            this.textBoxCurrentSensorAirTempDiff.TabIndex = 19;
            this.textBoxCurrentSensorAirTempDiff.TabStop = false;
            this.textBoxCurrentSensorAirTempDiff.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(201, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Relative Humidity";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(14, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Pressure";
            // 
            // textBoxCurrentSensorRH
            // 
            this.textBoxCurrentSensorRH.Location = new System.Drawing.Point(204, 32);
            this.textBoxCurrentSensorRH.Name = "textBoxCurrentSensorRH";
            this.textBoxCurrentSensorRH.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentSensorRH.TabIndex = 16;
            this.textBoxCurrentSensorRH.TabStop = false;
            this.textBoxCurrentSensorRH.Text = "0";
            // 
            // textBoxCurrentSensorPressure
            // 
            this.textBoxCurrentSensorPressure.Location = new System.Drawing.Point(17, 32);
            this.textBoxCurrentSensorPressure.Name = "textBoxCurrentSensorPressure";
            this.textBoxCurrentSensorPressure.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentSensorPressure.TabIndex = 12;
            this.textBoxCurrentSensorPressure.TabStop = false;
            this.textBoxCurrentSensorPressure.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(113, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Air Temperature";
            // 
            // textBoxCurrentSensorAirTemp
            // 
            this.textBoxCurrentSensorAirTemp.Location = new System.Drawing.Point(116, 32);
            this.textBoxCurrentSensorAirTemp.Name = "textBoxCurrentSensorAirTemp";
            this.textBoxCurrentSensorAirTemp.Size = new System.Drawing.Size(79, 20);
            this.textBoxCurrentSensorAirTemp.TabIndex = 14;
            this.textBoxCurrentSensorAirTemp.TabStop = false;
            this.textBoxCurrentSensorAirTemp.Text = "0";
            // 
            // groupBoxRadiosondePTUandGPS
            // 
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.label4);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.textBoxCurrentRadiosondeAlt);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.label5);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.textBoxCurrentRadiosondeLong);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.label6);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.textBoxCurrentRadiosondeLat);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.label3);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.textBoxCurrentRadiosondeRH);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.label2);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.textBoxCurrentRadiosondeAirTemp);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.label1);
            this.groupBoxRadiosondePTUandGPS.Controls.Add(this.textBoxCurrentRadiosondePressure);
            this.groupBoxRadiosondePTUandGPS.Location = new System.Drawing.Point(3, 212);
            this.groupBoxRadiosondePTUandGPS.Name = "groupBoxRadiosondePTUandGPS";
            this.groupBoxRadiosondePTUandGPS.Size = new System.Drawing.Size(651, 58);
            this.groupBoxRadiosondePTUandGPS.TabIndex = 7;
            this.groupBoxRadiosondePTUandGPS.TabStop = false;
            this.groupBoxRadiosondePTUandGPS.Text = "PTU && GPS Data from Radiosonde";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(535, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Altitude (m)";
            // 
            // textBoxCurrentRadiosondeAlt
            // 
            this.textBoxCurrentRadiosondeAlt.Location = new System.Drawing.Point(538, 31);
            this.textBoxCurrentRadiosondeAlt.Name = "textBoxCurrentRadiosondeAlt";
            this.textBoxCurrentRadiosondeAlt.Size = new System.Drawing.Size(60, 20);
            this.textBoxCurrentRadiosondeAlt.TabIndex = 10;
            this.textBoxCurrentRadiosondeAlt.TabStop = false;
            this.textBoxCurrentRadiosondeAlt.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(417, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Longitude (deg)";
            // 
            // textBoxCurrentRadiosondeLong
            // 
            this.textBoxCurrentRadiosondeLong.Location = new System.Drawing.Point(420, 31);
            this.textBoxCurrentRadiosondeLong.Name = "textBoxCurrentRadiosondeLong";
            this.textBoxCurrentRadiosondeLong.Size = new System.Drawing.Size(110, 20);
            this.textBoxCurrentRadiosondeLong.TabIndex = 8;
            this.textBoxCurrentRadiosondeLong.TabStop = false;
            this.textBoxCurrentRadiosondeLong.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(299, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Latitude (deg)";
            // 
            // textBoxCurrentRadiosondeLat
            // 
            this.textBoxCurrentRadiosondeLat.Location = new System.Drawing.Point(302, 31);
            this.textBoxCurrentRadiosondeLat.Name = "textBoxCurrentRadiosondeLat";
            this.textBoxCurrentRadiosondeLat.Size = new System.Drawing.Size(110, 20);
            this.textBoxCurrentRadiosondeLat.TabIndex = 6;
            this.textBoxCurrentRadiosondeLat.TabStop = false;
            this.textBoxCurrentRadiosondeLat.Tag = "";
            this.textBoxCurrentRadiosondeLat.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(201, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Relative Humidity";
            // 
            // textBoxCurrentRadiosondeRH
            // 
            this.textBoxCurrentRadiosondeRH.Location = new System.Drawing.Point(204, 31);
            this.textBoxCurrentRadiosondeRH.Name = "textBoxCurrentRadiosondeRH";
            this.textBoxCurrentRadiosondeRH.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentRadiosondeRH.TabIndex = 4;
            this.textBoxCurrentRadiosondeRH.TabStop = false;
            this.textBoxCurrentRadiosondeRH.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Air Temperature";
            // 
            // textBoxCurrentRadiosondeAirTemp
            // 
            this.textBoxCurrentRadiosondeAirTemp.Location = new System.Drawing.Point(116, 31);
            this.textBoxCurrentRadiosondeAirTemp.Name = "textBoxCurrentRadiosondeAirTemp";
            this.textBoxCurrentRadiosondeAirTemp.Size = new System.Drawing.Size(80, 20);
            this.textBoxCurrentRadiosondeAirTemp.TabIndex = 2;
            this.textBoxCurrentRadiosondeAirTemp.TabStop = false;
            this.textBoxCurrentRadiosondeAirTemp.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Pressure";
            // 
            // textBoxCurrentRadiosondePressure
            // 
            this.textBoxCurrentRadiosondePressure.Location = new System.Drawing.Point(17, 31);
            this.textBoxCurrentRadiosondePressure.Name = "textBoxCurrentRadiosondePressure";
            this.textBoxCurrentRadiosondePressure.Size = new System.Drawing.Size(90, 20);
            this.textBoxCurrentRadiosondePressure.TabIndex = 0;
            this.textBoxCurrentRadiosondePressure.TabStop = false;
            this.textBoxCurrentRadiosondePressure.Text = "0";
            // 
            // groupBoxRadiosondeFinalTest
            // 
            this.groupBoxRadiosondeFinalTest.Controls.Add(this.buttonLoadWorkOrder);
            this.groupBoxRadiosondeFinalTest.Controls.Add(this.labelCurrentLogFile);
            this.groupBoxRadiosondeFinalTest.Controls.Add(this.label52);
            this.groupBoxRadiosondeFinalTest.Controls.Add(this.label99);
            this.groupBoxRadiosondeFinalTest.Controls.Add(this.labelCurrentOperator);
            this.groupBoxRadiosondeFinalTest.Controls.Add(this.label41);
            this.groupBoxRadiosondeFinalTest.Controls.Add(this.labelCurrentWorkOrder);
            this.groupBoxRadiosondeFinalTest.Location = new System.Drawing.Point(3, 3);
            this.groupBoxRadiosondeFinalTest.Name = "groupBoxRadiosondeFinalTest";
            this.groupBoxRadiosondeFinalTest.Size = new System.Drawing.Size(651, 70);
            this.groupBoxRadiosondeFinalTest.TabIndex = 0;
            this.groupBoxRadiosondeFinalTest.TabStop = false;
            this.groupBoxRadiosondeFinalTest.Text = "Radiosonde Final Test";
            // 
            // buttonLoadWorkOrder
            // 
            this.buttonLoadWorkOrder.Location = new System.Drawing.Point(443, 18);
            this.buttonLoadWorkOrder.Name = "buttonLoadWorkOrder";
            this.buttonLoadWorkOrder.Size = new System.Drawing.Size(29, 23);
            this.buttonLoadWorkOrder.TabIndex = 14;
            this.buttonLoadWorkOrder.Text = "<";
            this.buttonLoadWorkOrder.UseVisualStyleBackColor = true;
            this.buttonLoadWorkOrder.Click += new System.EventHandler(this.buttonLoadWorkOrder_Click);
            // 
            // labelCurrentLogFile
            // 
            this.labelCurrentLogFile.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelCurrentLogFile.Location = new System.Drawing.Point(49, 46);
            this.labelCurrentLogFile.Name = "labelCurrentLogFile";
            this.labelCurrentLogFile.Size = new System.Drawing.Size(596, 18);
            this.labelCurrentLogFile.TabIndex = 2;
            this.labelCurrentLogFile.Text = "No Current Log File.";
            this.labelCurrentLogFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 49);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(47, 13);
            this.label52.TabIndex = 3;
            this.label52.Text = "Log File:";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(479, 24);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(48, 13);
            this.label99.TabIndex = 13;
            this.label99.Text = "Operator";
            // 
            // labelCurrentOperator
            // 
            this.labelCurrentOperator.BackColor = System.Drawing.Color.White;
            this.labelCurrentOperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelCurrentOperator.Location = new System.Drawing.Point(533, 21);
            this.labelCurrentOperator.Name = "labelCurrentOperator";
            this.labelCurrentOperator.Size = new System.Drawing.Size(110, 18);
            this.labelCurrentOperator.TabIndex = 12;
            this.labelCurrentOperator.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCurrentOperator.DoubleClick += new System.EventHandler(this.labelCurrentOperator_DoubleClick);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 24);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(62, 13);
            this.label41.TabIndex = 11;
            this.label41.Text = "Work Order";
            // 
            // labelCurrentWorkOrder
            // 
            this.labelCurrentWorkOrder.BackColor = System.Drawing.Color.White;
            this.labelCurrentWorkOrder.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelCurrentWorkOrder.Location = new System.Drawing.Point(70, 21);
            this.labelCurrentWorkOrder.Name = "labelCurrentWorkOrder";
            this.labelCurrentWorkOrder.Size = new System.Drawing.Size(367, 18);
            this.labelCurrentWorkOrder.TabIndex = 10;
            this.labelCurrentWorkOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCurrentWorkOrder.DoubleClick += new System.EventHandler(this.labelCurrentWorkOrder_DoubleClick);
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.printToolStripMenuItem,
            this.modeToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(784, 24);
            this.menuStripMain.TabIndex = 1;
            this.menuStripMain.Text = "menuStripMain";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newFileToolStripMenuItem,
            this.openFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newFileToolStripMenuItem
            // 
            this.newFileToolStripMenuItem.Name = "newFileToolStripMenuItem";
            this.newFileToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.newFileToolStripMenuItem.Text = "New File";
            // 
            // openFileToolStripMenuItem
            // 
            this.openFileToolStripMenuItem.Name = "openFileToolStripMenuItem";
            this.openFileToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.openFileToolStripMenuItem.Text = "Open File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testPrintToolStripMenuItem});
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.printToolStripMenuItem.Text = "Print";
            // 
            // testPrintToolStripMenuItem
            // 
            this.testPrintToolStripMenuItem.Name = "testPrintToolStripMenuItem";
            this.testPrintToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.testPrintToolStripMenuItem.Text = "Test Print";
            this.testPrintToolStripMenuItem.Click += new System.EventHandler(this.testPrintToolStripMenuItem_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.autoTestToolStripMenuItem,
            this.autoCableTestToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // autoTestToolStripMenuItem
            // 
            this.autoTestToolStripMenuItem.Name = "autoTestToolStripMenuItem";
            this.autoTestToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.autoTestToolStripMenuItem.Text = "Auto Start Test";
            this.autoTestToolStripMenuItem.Click += new System.EventHandler(this.autoTestToolStripMenuItem_Click);
            // 
            // autoCableTestToolStripMenuItem
            // 
            this.autoCableTestToolStripMenuItem.Name = "autoCableTestToolStripMenuItem";
            this.autoCableTestToolStripMenuItem.Size = new System.Drawing.Size(158, 22);
            this.autoCableTestToolStripMenuItem.Text = "Auto Cable Test";
            this.autoCableTestToolStripMenuItem.Click += new System.EventHandler(this.autoCableTestToolStripMenuItem_Click);
            // 
            // statusStripMain
            // 
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelMain,
            this.toolStripStatusLabelSondeBattery});
            this.statusStripMain.Location = new System.Drawing.Point(0, 540);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(784, 22);
            this.statusStripMain.TabIndex = 2;
            this.statusStripMain.Text = "statusStripMain";
            // 
            // toolStripStatusLabelMain
            // 
            this.toolStripStatusLabelMain.AutoSize = false;
            this.toolStripStatusLabelMain.Name = "toolStripStatusLabelMain";
            this.toolStripStatusLabelMain.Size = new System.Drawing.Size(660, 17);
            this.toolStripStatusLabelMain.Text = "Ready....";
            this.toolStripStatusLabelMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripStatusLabelSondeBattery
            // 
            this.toolStripStatusLabelSondeBattery.AutoSize = false;
            this.toolStripStatusLabelSondeBattery.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabelSondeBattery.Name = "toolStripStatusLabelSondeBattery";
            this.toolStripStatusLabelSondeBattery.Size = new System.Drawing.Size(100, 17);
            this.toolStripStatusLabelSondeBattery.Text = "No Data";
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panelLeft.Location = new System.Drawing.Point(0, 27);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(784, 510);
            this.panelLeft.TabIndex = 3;
            // 
            // frmMain
            // 
            this.AcceptButton = this.buttonStartTest;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.panelMainDisplay);
            this.Controls.Add(this.menuStripMain);
            this.Controls.Add(this.panelLeft);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "frmMain";
            this.Text = "600085.0003 Radiosonde Final Check 3 1.0.0.46";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.panelMainDisplay.ResumeLayout(false);
            this.groupBoxPassCount.ResumeLayout(false);
            this.groupBoxAuxCableTest.ResumeLayout(false);
            this.panelAuxCable.ResumeLayout(false);
            this.groupBoxSensorStability.ResumeLayout(false);
            this.groupBoxSensorStability.PerformLayout();
            this.groupBoxRadiosondeInformation.ResumeLayout(false);
            this.groupBoxRadiosondeInformation.PerformLayout();
            this.groupBoxAmbientValues.ResumeLayout(false);
            this.groupBoxAmbientValues.PerformLayout();
            this.groupBoxRadiosondePTUandGPS.ResumeLayout(false);
            this.groupBoxRadiosondePTUandGPS.PerformLayout();
            this.groupBoxRadiosondeFinalTest.ResumeLayout(false);
            this.groupBoxRadiosondeFinalTest.PerformLayout();
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelMainDisplay;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxRadiosondeFinalTest;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.ComboBox comboBoxRadiosondeType;
        private System.Windows.Forms.Button buttonStartTest;
        private System.Windows.Forms.Label labelCurrentOperator;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label labelCurrentWorkOrder;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxAuxCableTest;
        private System.Windows.Forms.Panel panelAuxCable;
        private System.Windows.Forms.Label labelCableTestResults;
        private System.Windows.Forms.Label labelCableTestStatus;
        private System.Windows.Forms.Button buttonTestAuxCable;
        private System.Windows.Forms.GroupBox groupBoxSensorStability;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxSondePressureChange;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxRefPressureChange;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxSondeTempChange;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxRefTempChange;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxSondeHumidityChange;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxRefHumidityChange;
        private System.Windows.Forms.GroupBox groupBoxRadiosondeInformation;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeFirmware;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeTUNumber;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeTrackingNumber;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeSerialNumber;
        private System.Windows.Forms.GroupBox groupBoxAmbientValues;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox textBoxCurrentSensorRHDiff;
        private System.Windows.Forms.TextBox textBoxCurrentSensorPressureDiff;
        private System.Windows.Forms.TextBox textBoxCurrentSensorAirTempDiff;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxCurrentSensorRH;
        private System.Windows.Forms.TextBox textBoxCurrentSensorPressure;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxCurrentSensorAirTemp;
        private System.Windows.Forms.GroupBox groupBoxRadiosondePTUandGPS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeAlt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeLong;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeLat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeRH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondeAirTemp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxCurrentRadiosondePressure;
        private System.Windows.Forms.Label labelCurrentLogFile;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ToolStripMenuItem newFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testPrintToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoCableTestToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelMain;
        private System.Windows.Forms.GroupBox groupBoxPassCount;
        private System.Windows.Forms.Label labelPassCounter;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelSondeBattery;
        private System.Windows.Forms.Button buttonLoadWorkOrder;
    }
}

