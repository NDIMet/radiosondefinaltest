﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FinalCheck3
{
    public partial class frmSettings : Form
    {
        public frmSettings()
        {
            InitializeComponent();

            displayCurrentSettings();
            updateComboBoxItems();
        }

        #region Methods for setting up the display
        private void displayCurrentSettings()
        {
            comboBoxBell202.Text = Program.currentProgramSettings.decoderBell202;
            comboBoxEE31.Text = Program.currentProgramSettings.sensorEE31;
            comboBoxHMP234.Text = Program.currentProgramSettings.sensorHMP234;
            comboBoxBaro.Text = Program.currentProgramSettings.sensorBaro;
            comboBoxAuxCable.Text = Program.currentProgramSettings.mfgConAuxCable;
            comboBoxIMSDecoder.Text = Program.currentProgramSettings.decoderIMet1;
            comboBoxMFGCable.Text = Program.currentProgramSettings.mfgConMFGConfig;
            comboBoxShippingPrinter.Text = Program.currentProgramSettings.printerShipping;
            comboBoxSNPrinter.Text = Program.currentProgramSettings.printerSN;

            textBoxMasterLog.Text = Program.currentProgramSettings.masterLog;
            textBoxWODir.Text = Program.currentProgramSettings.baseDirWOLog;
            textBoxXCalDirectory.Text = Program.currentProgramSettings.xcalLOC;

            textBoxBiasP.Text = Program.currentTestSettings.offsetPressure.ToString("0.00");
            textBoxBiasTemp.Text = Program.currentTestSettings.offsetAirTemp.ToString("0.00");
            textBoxBiasHumidity.Text = Program.currentTestSettings.offsetHumidity.ToString("0.00");

            textBoxStabilityP.Text = Program.currentTestSettings.MSLrefP.ToString("0.00");
            textBoxStabilityT.Text = Program.currentTestSettings.MSLrefT.ToString("0.00");
            textBoxStabilityU.Text = Program.currentTestSettings.MSLrefU.ToString("0.00");

            //Radiosonde test tolerance
            textBoxPressureTol.Text = Program.currentTestSettings.tolPressure.ToString();
            textBoxTempTol.Text = Program.currentTestSettings.tolAirTemp.ToString();
            textBoxHumidityTol.Text = Program.currentTestSettings.tolHumidity.ToString();
            textBoxGPSHorzTol.Text = Program.currentTestSettings.tolHorizonal.ToString();
            textBoxGPSVerTol.Text = Program.currentTestSettings.tolVertical.ToString();
        }

        private void updateComboBoxItems()
        {
            string[] allComs = System.IO.Ports.SerialPort.GetPortNames();   //Pulling in system com ports
            string[] totalComs = new string[allComs.Length + 1];            //Setting up a total index of settings will null.

            totalComs[0] = "NULL";
            for (int i = 1; i < totalComs.Length; i++)  //Compileing a list with the null to be able to disable.
            {
                totalComs[i] = allComs[i - 1];
            }


            foreach (string com in totalComs)   //Filling up the combo box.
            {
                comboBoxAuxCable.Items.Add(com);
                comboBoxBaro.Items.Add(com);
                comboBoxBell202.Items.Add(com);
                comboBoxEE31.Items.Add(com);
                comboBoxHMP234.Items.Add(com);
                comboBoxIMSDecoder.Items.Add(com);
                comboBoxMFGCable.Items.Add(com);
            }


            comboBoxShippingPrinter.Items.Add("NULL");
            comboBoxSNPrinter.Items.Add("NULL");
            foreach (string printers in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                comboBoxShippingPrinter.Items.Add(printers);
                comboBoxSNPrinter.Items.Add(printers);
            }

        }

        #endregion

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Button saves and applies them to the current program.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {
            saveSettings();     //Saving setting from the screen.
        }

        private void saveSettings()
        {
            //Loading the current settings file.
            string[] settings = System.IO.File.ReadAllLines(Program.settingsFile);

            //Updating the data in memory.
            for (int i = 0; i < settings.Length; i++)
            {
                if (!settings[i].Contains("#")) //Ignoring lines that contain #.
                {
                    if (settings[i].Contains("offset=pressure"))
                    {
                        settings[i] = "offset=pressure," + textBoxBiasP.Text;
                        Program.sensorBaro.setBias(Convert.ToDouble(textBoxBiasP.Text));
                        Program.currentTestSettings.offsetPressure = Convert.ToDouble(textBoxBiasP.Text);
                    }

                    if (settings[i].Contains("offset=airTemp"))
                    {
                        settings[i] = "offset=airTemp," + textBoxBiasTemp.Text;
                        if (Program.sensorEE31 != null)
                        {
                            Program.sensorEE31.setAirTempBias(Convert.ToDouble(textBoxBiasTemp.Text));
                            Program.currentTestSettings.offsetAirTemp = Convert.ToDouble(textBoxBiasTemp.Text);
                        }
                        //If needed add the HMP234 Bias.
                    }

                    if (settings[i].Contains("offset=humidity"))
                    {
                        settings[i] = "offset=humidity," + textBoxBiasHumidity.Text;
                        if (Program.sensorEE31 != null)
                        {
                            Program.sensorEE31.setHumidityBias(Convert.ToDouble(textBoxBiasHumidity.Text));
                            Program.currentTestSettings.offsetHumidity = Convert.ToDouble(textBoxBiasHumidity.Text);
                        }
                    }

                    //Max stability
                    if (settings[i].Contains("maxStability=pressure"))
                    {
                        settings[i] = "maxStability=pressure," + textBoxStabilityP.Text;
                        Program.currentTestSettings.MSLrefP = Convert.ToDouble(textBoxStabilityP.Text);
                        Program.currentTestSettings.MSLsondeP = Convert.ToDouble(textBoxStabilityP.Text);
                    }

                    if (settings[i].Contains("maxStability=airTemp"))
                    {
                        settings[i] = "maxStability=airTemp," + textBoxStabilityT.Text;
                        Program.currentTestSettings.MSLrefT = Convert.ToDouble(textBoxStabilityT.Text);
                        Program.currentTestSettings.MSLsondeT = Convert.ToDouble(textBoxStabilityT.Text);
                    }

                    if (settings[i].Contains("maxStability=humidity"))
                    {
                        settings[i] = "maxStability=humidity," + textBoxStabilityU.Text;
                        Program.currentTestSettings.MSLrefU = Convert.ToDouble(textBoxStabilityU.Text);
                        Program.currentTestSettings.MSLsondeU = Convert.ToDouble(textBoxStabilityU.Text);
                    }

                    if (settings[i].Contains("printer,snlabel="))
                    {
                        settings[i] = "printer,snlabel=" + comboBoxSNPrinter.Text;
                        Program.currentProgramSettings.printerSN = comboBoxSNPrinter.Text;
                        Program.setupSNPrinter();
                    }

                    //Applying the radiosonde testing settings.
                    if (settings[i].Contains("tol=pressure,"))
                    {
                        settings[i] = "tol=pressure," + textBoxPressureTol.Text;
                        Program.currentTestSettings.tolPressure = Convert.ToDouble(textBoxPressureTol.Text);
                    }

                    if (settings[i].Contains("tol=airTemp,"))
                    {
                        settings[i] = "tol=airTemp," + textBoxTempTol.Text;
                        Program.currentTestSettings.tolAirTemp = Convert.ToDouble(textBoxTempTol.Text);
                    }

                    if (settings[i].Contains("tol=humidity,"))
                    {
                        settings[i] = "tol=humidity," + textBoxHumidityTol.Text;
                        Program.currentTestSettings.tolHumidity = Convert.ToDouble(textBoxHumidityTol.Text);
                    }

                    if (settings[i].Contains("tol=horizonal,"))
                    {
                        settings[i] = "tol=horizonal," + textBoxGPSHorzTol.Text;
                        Program.currentTestSettings.tolHorizonal = Convert.ToDouble(textBoxGPSHorzTol.Text);
                    }

                    if (settings[i].Contains("tol=vertical,"))
                    {
                        settings[i] = "tol=vertical," + textBoxGPSVerTol.Text;
                        Program.currentTestSettings.tolVertical = Convert.ToDouble(textBoxGPSVerTol.Text);
                    }


                    //Saving hardware.
                    //Sensors
                    if (settings[i].Contains("sensor=TU,EE31,"))
                    {
                        settings[i] = "sensor=TU,EE31," + comboBoxEE31.Text;
                        Program.currentProgramSettings.sensorEE31 = comboBoxEE31.Text;
                    }

                    if (settings[i].Contains("sensor=P,youngbaro,"))
                    {
                        settings[i] = "sensor=P,youngbaro," + comboBoxBaro.Text;
                        Program.currentProgramSettings.sensorBaro = comboBoxBaro.Text;
                    }

                    if (settings[i].Contains("sensor=TU,HMP234,"))
                    {
                        settings[i] = "sensor=TU,HMP234," + comboBoxHMP234.Text;
                        Program.currentProgramSettings.sensorHMP234 = comboBoxHMP234.Text;
                    }

                    //Decoders
                    if (settings[i].Contains("decoder=bell202,"))
                    {
                        settings[i] = "decoder=bell202," + comboBoxBell202.Text;
                        Program.currentProgramSettings.decoderBell202 = comboBoxBell202.Text;
                    }

                    if (settings[i].Contains("decoder=imet1,"))
                    {
                        settings[i] = "decoder=imet1," + comboBoxIMSDecoder.Text;
                        Program.currentProgramSettings.decoderIMet1 = comboBoxIMSDecoder.Text;
                    }

                    //MFG cables and aux
                    if (settings[i].Contains("mfgConn=auxcable,"))
                    {
                        settings[i] = "mfgConn=auxcable," + comboBoxAuxCable.Text;
                        Program.currentProgramSettings.mfgConAuxCable = comboBoxAuxCable.Text;
                    }

                    if (settings[i].Contains("mfgConn=mfgConfig,"))
                    {
                        settings[i] = "mfgConn=mfgConfig," + comboBoxMFGCable.Text;
                        Program.currentProgramSettings.mfgConMFGConfig = comboBoxMFGCable.Text;
                    }

                    //Logging locations
                    if (settings[i].Contains("masterLog="))
                    {
                        settings[i] = "masterLog=" + textBoxMasterLog.Text;
                        Program.currentProgramSettings.masterLog = textBoxMasterLog.Text;
                    }

                    if (settings[i].Contains("logBaseDir="))
                    {
                        settings[i] = "logBaseDir=" + textBoxWODir.Text;
                        Program.currentProgramSettings.baseDirWOLog = textBoxWODir.Text;
                    }

                    if (settings[i].Contains("xcalLOC="))
                    {
                        settings[i] = "xcalLOC=" + textBoxXCalDirectory.Text;
                        Program.currentProgramSettings.xcalLOC = textBoxXCalDirectory.Text;
                    }
                }
            }

            //Writing the data to the settings file.
            System.IO.File.WriteAllLines(Program.settingsFile, settings);

            //Program.connectToHardware(); //reload program setting to sensors, decoders, and printers.
        }

        private void buttonMasterLog_Click(object sender, EventArgs e)
        {
            string data;
            SaveFileDialog fileDialog = new SaveFileDialog();
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                data = fileDialog.FileName;
                textBoxMasterLog.Text = data;
            }
            else
            {
                return;
            }

            updateSettingLine("masterLog=", textBoxMasterLog.Text);
            Program.currentProgramSettings.masterLog = data;

        }

        private void buttonXCalDirectory_Click(object sender, EventArgs e)
        {
            string data;
            if ((data = getDirLoc()) != null)
            {
                textBoxXCalDirectory.Text = data;
            }
            else
            {
                return;
            }

            updateSettingLine("xcalLOC=", textBoxXCalDirectory.Text + "\\");
            Program.currentProgramSettings.xcalLOC = data;
        }

        private void buttonWODir_Click(object sender, EventArgs e)
        {
            string data;
            if ((data = getDirLoc()) != null)
            {
                textBoxWODir.Text = data;
            }
            else
            {
                return;
            }

            updateSettingLine("logBaseDir=", textBoxWODir.Text);
            Program.currentProgramSettings.baseDirWOLog = data;
        }

        string getDirLoc()
        {
            FolderBrowserDialog getFolder = new FolderBrowserDialog();
            if(getFolder.ShowDialog() == DialogResult.OK)
            {
                return getFolder.SelectedPath;
            }
            else
            {
                return null;
            }
        }

        void updateSettingLine(string searchLine, string data)
        {
            string[] file = System.IO.File.ReadAllLines(Program.settingsFile);

            for (int i = 0; i < file.Length; i++)
            {
                if (file[i].Contains(searchLine))
                {
                    file[i] = searchLine + data;
                }
            }

            System.IO.File.WriteAllLines(Program.settingsFile, file);
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            //Closing the settings form.
            this.Close();
        }

    }
}
