﻿///This is a test program for testing radiosondes at final test.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace testFinalTest
{
    class Program
    {
        static TestEquipment.epluseEE31 sensorEPlusE = new TestEquipment.epluseEE31();
        static TestEquipment.HMP234 sensorHMP;
        static TestEquipment.YoungBaro sensorPressure;

        static txDecoder.decoderBell202 decoderBell202;
        static iMet_1_Decoder.iMet1_Decoder decoderIMS;

        //Test conditions
        static FinalTest.testParameters currentTest;
        static FinalTest.radiosondeTest testRadiosonde;

        //Test sensors and radiosondes workers
        static System.ComponentModel.BackgroundWorker backWorkPressure = new System.ComponentModel.BackgroundWorker();
        static System.ComponentModel.BackgroundWorker backWorkTU = new System.ComponentModel.BackgroundWorker();
        static Random instability = new Random();


        //Testing enviroment
        static double currentAirTemp = 20;
        static int airTempInstability = 1;

        static double currentHumidity = 30;
        static int humidityInstability = 1;

        static double currentPressure = 990;
        static int pressureInstability = 1;
        
        //Stability stuff.....  Bitches love stability
        static List<FinalTest.stabilityElement> elements = new List<FinalTest.stabilityElement>();


        static void Main(string[] args)
        {
            //radiosondeTest(object decoderData, Universal_Radiosonde.iMet1U cableData, TestEquipment.YoungBaro sensorPressure, TestEquipment.epluseEE31 sensorEplusE)

            decoderBell202 = new txDecoder.decoderBell202();
            decoderBell202.packetPTUXReady.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(packetPTUXReady_PropertyChange);

            sensorPressure = new TestEquipment.YoungBaro();
            sensorEPlusE = new TestEquipment.epluseEE31();


            FinalTest.stabilityElement sbPressure = new FinalTest.stabilityElement();
            sbPressure.stabilityElementSetup("Ref Pressure", 30);
            elements.Add(sbPressure);

            FinalTest.stabilityElement sbAirTemp = new FinalTest.stabilityElement();
            sbAirTemp.stabilityElementSetup("Ref Air Temp", 30);
            elements.Add(sbAirTemp);

            FinalTest.stabilityElement sbHumidity = new FinalTest.stabilityElement();
            sbHumidity.stabilityElementSetup("Ref Humidity", 30);
            elements.Add(sbHumidity);

            backWorkPressure.DoWork += new System.ComponentModel.DoWorkEventHandler(backWorkPressure_DoWork);
            backWorkTU.DoWork += new System.ComponentModel.DoWorkEventHandler(backWorkTU_DoWork);   

            backWorkPressure.RunWorkerAsync();
            backWorkTU.RunWorkerAsync();

            //Setting up the current test parm.
            currentTest.maxTimeToStablize = 4;
            currentTest.numberOfCorrect = 30;
            currentTest.optionGPS = true;
            currentTest.optionPressure = true;
            currentTest.optionAuxCable = false;
            currentTest.stationAlt = 210;
            currentTest.stationLat = 42.8937776852398;
            currentTest.stationLong = -85.5701756570488;
            currentTest.tolAirTemp = .2;
            currentTest.tolHumidity = 5;
            currentTest.tolPressure = 1.8;
            currentTest.tolHorizonal = 20;
            currentTest.tolVertical = 20;
            currentTest.MSLrefP = .2;
            currentTest.MSLrefT = .2;
            currentTest.MSLrefT = .2;
            currentTest.MSLsondeP = .2;
            currentTest.MSLsondeT = .2;
            currentTest.MSLsondeU = .2;

            testRadiosonde = new FinalTest.radiosondeTest(decoderBell202, null, sensorPressure, sensorEPlusE, currentTest, elements);

            while (true)
            {
                string commmand = Console.ReadLine();

                switch (commmand)
                {
                    case "sondeon":
                        decoderBell202.setTestMode(true);
                        Console.WriteLine("Sonde Power On");
                        break;

                    case "sondeoff":
                        decoderBell202.setTestMode(false);
                        Console.WriteLine("Sonde Power Off");
                        break;

                    case "startTest":
                        testRadiosonde = new FinalTest.radiosondeTest(decoderBell202, null, sensorPressure, sensorEPlusE, currentTest, elements);
                        testRadiosonde.testError.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(testError_PropertyChange);
                        testRadiosonde.updatePacket.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(updatePacket_PropertyChange);
                        testRadiosonde.testComplete.PropertyChange += new updateCreater.objectUpdate.PropertyChangeHandler(testComplete_PropertyChange);
                        testRadiosonde.startTest();
                        Console.WriteLine("Test Started");
                        break;

                    case "stopTest":
                        testRadiosonde.stopTest();
                        testRadiosonde = null;
                        Console.WriteLine("Test Stopped");
                        break;

                    case "setPTU":
                        Console.WriteLine("Setting PTU Test:");
                        Console.Write("P Tol>");
                        currentTest.tolPressure = Convert.ToDouble(Console.ReadLine());

                        Console.Write("T Tol>");
                        currentTest.tolAirTemp = Convert.ToDouble(Console.ReadLine());

                        Console.Write("U Tol>");
                        currentTest.tolHumidity = Convert.ToDouble(Console.ReadLine());
                        break;

                    case "sondeStability":
                        Console.WriteLine("Set Sonde Instability:(Pressure),(Air Temp),(Humidity),(GPSX),(GPSAlt)");
                        string incomingSetting = Console.ReadLine();
                        string[] brokeDown = incomingSetting.Split(',');
                        decoderBell202.adjustInstability(Convert.ToInt16(brokeDown[0]), Convert.ToInt16(brokeDown[1]), Convert.ToInt16(brokeDown[2]), Convert.ToInt16(brokeDown[3]), Convert.ToInt16(brokeDown[4]));

                        break;

                }
            }
        }

        static void testComplete_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            bool results = (bool)data.NewValue;
            Console.WriteLine("=========================================");
            Console.WriteLine("=========================================");
            Console.WriteLine(results);
            Console.WriteLine("=========================================");
            Console.WriteLine("=========================================");
        }

        static void updatePacket_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            FinalTest.testResults status = (FinalTest.testResults)data.NewValue;
            Console.WriteLine(status.statusPressure + "," + status.statusAirTemp + "," + status.statusHumidity + "," + status.statusCable + "," + status.statusGPSPOS);

            foreach (FinalTest.stabilityElement el in elements)
            {
                Console.WriteLine(el.desciption + "=" + el.getBoxcarAverage().ToString("0.00") + " ");
            }

            Console.WriteLine("------------------------------------------");

            if (testRadiosonde != null)
            {
                foreach (FinalTest.stabilityElement check in testRadiosonde.testingEnviroment)
                {
                    Console.WriteLine(check.desciption + "=" + check.getBoxcarAverage().ToString("0.00"));
                }
            }

            Console.Write("\n");
        }

        static void testError_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            System.Exception incomingData = (System.Exception)data.NewValue;
            Console.WriteLine(incomingData.Message);
        }

        static void packetPTUXReady_PropertyChange(object sender, updateCreater.PropertyChangeEventArgs data)
        {
            txDecoder.radiosondePTUData incoming = decoderBell202.getLatestPTU();
            Console.WriteLine(incoming.packetNumber + " - " + incoming.pressure.ToString("0.00") + "," + incoming.airTemp.ToString("0.00") + "," + incoming.humidity.ToString("0.00"));

            /*
            #if DEBUG
            foreach (FinalTest.stabilityElement el in elements)
            {
                Console.WriteLine(el.desciption + " - " + el.getDiff(60).ToString("0.000") + " : " + el.getBoxcarAverage().ToString("0.000"));
            }
            #endif
             */ 
        }


        #region Backgound workers for generating referance sensor data.
        static void backWorkTU_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!backWorkTU.CancellationPending)
            {

                sensorEPlusE.testTempInput(currentAirTemp * (((Convert.ToDouble(instability.Next(0, airTempInstability * 10)) / 10000) * currentInstabilityDir()) + 1));

                foreach (FinalTest.stabilityElement item in elements)
                {
                    if (item.desciption.ToLower().Contains("ref") && item.desciption.ToLower().Contains("air temp"))
                    {
                        item.addDataPoint(sensorEPlusE.currentAirTemp);
                    }
                }

                sensorEPlusE.testHumidityInput(currentHumidity * (((Convert.ToDouble(instability.Next(0, humidityInstability * 10)) / 10000) * currentInstabilityDir()) + 1));

                foreach (FinalTest.stabilityElement item in elements)
                {
                    if (item.desciption.ToLower().Contains("ref") && item.desciption.ToLower().Contains("humidity"))
                    {
                        item.addDataPoint(sensorEPlusE.currentHumidity);
                    }
                }


                System.Threading.Thread.Sleep(1000);
            }
        }

        static void backWorkPressure_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!backWorkPressure.CancellationPending)
            {

                sensorPressure.testPressureInput(currentPressure * (((Convert.ToDouble(instability.Next(0,pressureInstability * 10))/100000) * currentInstabilityDir()) + 1));

                foreach (FinalTest.stabilityElement item in elements)
                {
                    if (item.desciption.ToLower().Contains("ref") && item.desciption.ToLower().Contains("pressure"))
                    {
                        item.addDataPoint(sensorPressure.CurrentPressure);
                    }
                }

                System.Threading.Thread.Sleep(1000);
            }
        }

        #endregion


        static int currentInstabilityDir()
        {
            int curtDir = -1;
            int cNum = instability.Next(0, 2);
            if (cNum == 1)
            {
                curtDir = 1;
            }
            return curtDir;
        }

    }
}
